@echo off

set RELEASE_DIR="..\\release\\FrameVis"
set EXE_DIR="..\\src\\FrameVis"
set EXE_FILE="FrameVis.exe"
set CUR_DIR="%cd%"

rd /s /q %RELEASE_DIR%
md %RELEASE_DIR%

copy "%EXE_DIR%\\%EXE_FILE%" "%RELEASE_DIR%" > nul

windeployqt --release --dir "%RELEASE_DIR%" "%EXE_DIR%\\%EXE_FILE%" > nul
