#pragma once

#include "interpolation.h"

class LinearInterpolation : public Interpolation {
public:
    LinearInterpolation(double min, double max);

protected:
    double interpolate(double value) const override;
};
