#include "log_interpolation.h"

#include <cmath>

LogInterpolation::LogInterpolation(double min, double max) :
    Interpolation(min, max) {
}

double LogInterpolation::interpolate(double value) const {
    const auto denom = getDistance() > 0.0 ? log(getDistance() + 1.0) : 1.0;
    return log(value - getMin() + 1.0)/denom;
}
