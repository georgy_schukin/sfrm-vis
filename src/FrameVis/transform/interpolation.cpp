#include "interpolation.h"

Interpolation::Interpolation(double min, double max) :
    min(min), max(max) {
}

void Interpolation::setMin(double min) {
    this->min = min;
}

void Interpolation::setMax(double max) {
    this->max = max;
}

void Interpolation::update(double min, double max) {
    setMin(min);
    setMax(max);
}

double Interpolation::transform(double value) const {
    if (value < min) {
        return 0.0;
    } else if (value > max) {
        return 1.0;
    } else {
        return interpolate(value);
    }
}
