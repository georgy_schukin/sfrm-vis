#pragma once

class Transformation {
public:
    virtual ~Transformation() = default;

    virtual double transform(double value) const = 0;
};
