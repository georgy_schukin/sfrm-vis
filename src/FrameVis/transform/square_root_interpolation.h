#pragma once

#include "interpolation.h"

class SquareRootInterpolation : public Interpolation {
public:
    SquareRootInterpolation(double min, double max);

protected:
    double interpolate(double value) const override;
};
