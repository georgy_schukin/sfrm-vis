#pragma once

#include "interpolation.h"

class LogInterpolation : public Interpolation {
public:
    LogInterpolation(double min, double max);    

protected:
    double interpolate(double value) const override;
};
