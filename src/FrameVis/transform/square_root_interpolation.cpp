#include "square_root_interpolation.h"

#include <cmath>

SquareRootInterpolation::SquareRootInterpolation(double min, double max) :
    Interpolation(min, max) {
}

double SquareRootInterpolation::interpolate(double value) const {
    return sqrt(value - getMin()) / (getDistance() > 0.0 ? sqrt(getDistance()) : 1.0);
}
