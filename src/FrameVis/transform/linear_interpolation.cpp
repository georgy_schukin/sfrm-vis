#include "linear_interpolation.h"

LinearInterpolation::LinearInterpolation(double min, double max) :
    Interpolation(min, max) {
}

double LinearInterpolation::interpolate(double value) const {
    return (value - getMin()) / (getDistance() > 0.0 ? getDistance() : 1.0);
}
