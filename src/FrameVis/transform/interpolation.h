#pragma once

#include "transformation.h"

/**
 * @brief Convert value from [min, max] to [0, 1] interval.
 */
class Interpolation : public Transformation {
public:
    Interpolation(double min, double max);

    double getMin() const {
        return min;
    }

    double getMax() const {
        return max;
    }

    double getDistance() const {
        return max - min;
    }

    void setMin(double min);
    void setMax(double max);
    virtual void update(double min, double max);

    double transform(double value) const override;

protected:
    virtual double interpolate(double value) const = 0;

private:
    double min;
    double max;
};
