#pragma once

#include "frame_data.h"
#include "pick/pick.h"
#include "pick/pick_collection.h"

#include <QPixmap>
#include <QString>
#include <QPoint>

class Frame {
public:
    Frame();
    Frame(const FrameData &data);
    Frame(const FrameData &data, const QPixmap &pixmap);

    void setId(int id);
    void setData(const FrameData &data);
    void setPixmap(const QPixmap &pixmap);
    void setFilename(const QString &filename);

    int getId() const;
    const FrameData& getData() const;
    const QPixmap& getPixmap() const;
    QString getFilename() const;

    bool hasData() const;
    bool hasPixmap() const;

    bool hasValueAt(const QPoint &point) const;
    QString strValueAt(const QPoint &point) const;
    QString strValueAtFull(const QPoint &point) const;
    QString getName() const;

    double getMin() const;
    double getMax() const;

    Pick addPick(const QPoint &point);
    bool hasPick(const QPoint &point) const;
    Pick removePick(const QPoint &point);
    Pick changePick(const QPoint &point, const Pick &new_pick);
    const std::vector<Pick>& getPicks() const;
    void clearPicks();

private:
    int id;
    FrameData data;
    QPixmap pixmap;
    QString filename;
    PickCollection picks;
};
