#pragma once

#include <string>
#include <vector>

class Frame2D {
public:
    Frame2D() {}

    void read_sfrm(const std::string &fname);

    void set_data_dim(const int line_num, const int col_num);

    int calc_data_dim_lines();
    int calc_data_dim_columns();

    void show_data_rect(int l_min, int c_min, int l_max, int c_max);

    int get_num_of_rows() const {
        return Frame2D::m_dim_lines;
    }

    int get_num_of_columns() const {
        return Frame2D::m_dim_columns;
    }

    int get_data(const int row, const int col) const {
        return Frame2D::m_data[row][col];
    }

private:
    void read_sfrm_header(std::ifstream &sfrm_file);
    void read_sfrm_body(std::ifstream &sfrm_file);

private:
    std::vector< std::vector< int > > m_data; // lines< columns<> >
    int m_dim_lines_count = 0;
    int m_dim_colunms_count = 0;
    int m_dim_columns_min = 0;
    int m_dim_columns_max = 0;

    int m_dim_columns = 512;
    int m_dim_lines = 512;

    int m_counts_max = 0;
    int m_counts_min = 0;
    int m_counts_sum = 0;

    int m_overflow_flag = 0;
    int m_overflow_byte = 0;
    int m_overflow_word = 0;    
};

