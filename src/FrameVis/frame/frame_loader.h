#pragma once

#include "frame_data.h"

#include <string>

class FrameLoader {
public:
    static FrameData readSFRM(const std::string &filename);
};
