#pragma once

#include "frame.h"

#include <vector>
#include <QString>
#include <QPixmap>
#include <memory>

class Palette;
class Transformation;

class FrameCollection {
public:
    FrameCollection();
    ~FrameCollection() {}

    void addFrame(const FrameData &data);
    void addFrame(const FrameData &data, const QString &filename);
    void addFrame(const FrameData &data, const QPixmap &pixmap, const QString &filename);
    void addFrame(const Frame &frame);

    Frame* getFrame(int index) const;
    Frame* getFrameById(int id) const;
    Frame* getCurrentFrame() const;
    Frame* nextFrame(const Frame *frame);
    Frame* prevFrame(const Frame *frame);

    int getFrameIndex(const Frame *frame) const;

    int getNumOfFrames() const;

    void clear();

    void generatePixmaps(const Palette &palette, const Transformation &trans);

    double getMinValue() const;
    double getMaxValue() const;

private:
    std::vector<std::shared_ptr<Frame>> frames;
    int current_frame_index;
};
