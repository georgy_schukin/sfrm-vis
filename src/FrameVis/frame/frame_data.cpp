#include "frame/frame_data.h"
#include "frame/frame_loader.h"
#include "color/colorizer.h"
#include "color/palette.h"
#include "transform/transformation.h"

#include <algorithm>
#include <cmath>
#include <QImage>

FrameData::FrameData() :
    width(0), height(0) {
}

FrameData::FrameData(int width, int height, const std::vector<double> &data) :
    width(width), height(height), data(data) {
}

double FrameData::getMin() const {
    if (data.empty()) {
        return 0.0;
    }
    return *std::min_element(data.begin(), data.end());
}

double FrameData::getMax() const {
    if (data.empty()) {
        return 0.0;
    }
    return *std::max_element(data.begin(), data.end());
}

bool FrameData::isIn(int x, int y) const {
    return (x >=0 && x < width && y >= 0 && y < height);
}

double FrameData::at(int x, int y) const {
    if (!isIn(x, y)) {
        return 0.0;
    }
    return data[y*width + x];
}

bool FrameData::empty() const {
    return data.empty();
}

/*QPixmap FrameData::toPixmap(const Colorizer &colors) const {
    QImage img(width, height, QImage::Format_RGB888);
    img.fill(Qt::black);
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            const auto &value = data[y*width + x];
            img.setPixelColor(x, y, colors.lerp(value));
        }
    }
    return QPixmap::fromImage(img);
}

QPixmap FrameData::toPixmap(const Palette &palette, const Transformation &trans) const {
    return toPixmap(palette, getMin(), getMax());
}*/

QPixmap FrameData::toPixmap(const Palette &palette, const Transformation &trans) const {
    QImage img(width, height, QImage::Format_RGB32);
    img.fill(Qt::black);
    for (int y = 0; y < img.height(); y++) {
        auto *line = reinterpret_cast<QRgb*>(img.scanLine(y));
        for (int x = 0; x < img.width(); x++) {
            const auto value = data[y*width + x];
            const auto color = palette.getColor(trans.transform(value));
            line[x] = color.rgb();
        }
    }
    return QPixmap::fromImage(img);
}

FrameData FrameData::makeRandom(int width, int height, double min, double max) {
    std::vector<double> data(width*height);
    for (size_t i = 0; i < data.size(); i++) {
        data[i] = min + double(rand() % 1000)*(max - min)/1000.0;
    }
    return FrameData(width, height, data);
}

FrameData FrameData::makeCircle(int width, int height, double min, double max) {
    const auto radius = std::min(width/2, height/2);
    std::vector<double> data(width*height);
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            const auto dx = width/2 - j;
            const auto dy = height/2 - i;
            const auto dist = std::min(std::sqrt(dx*dx + dy*dy), double(radius));
            data[i*width + j] = min + dist*(max - min)/radius;
        }
    }
    return FrameData(width, height, data);
}

FrameData FrameData::makeRibbon(int width, int height, double min, double max) {
    std::vector<double> data(width*height);
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            data[i*width + j] = min + j*(max - min)/(width - 1);
        }
    }
    return FrameData(width, height, data);
}

FrameData FrameData::loadFromFile(const std::string &filename) {
    return FrameLoader::readSFRM(filename);
}
