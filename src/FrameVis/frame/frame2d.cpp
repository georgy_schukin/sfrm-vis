#include "frame2d.h"

#include <limits>
#include <fstream>
#include <string>
#include <sstream>

#include <iostream>
#include <iomanip>

int Frame2D::calc_data_dim_lines()
{
    m_dim_lines_count = m_data.size();
    return m_dim_lines_count;
}

int Frame2D::calc_data_dim_columns()
{
    int cur_dim;

    m_dim_columns_min = std::numeric_limits<int>::max();
    m_dim_columns_max = 0;
    for(int i = 0; i < m_data.size(); i++)
    {
        cur_dim = m_data[i].size();
        if (cur_dim > m_dim_columns_max) m_dim_columns_max = cur_dim;
        if (cur_dim < m_dim_columns_min) m_dim_columns_min = cur_dim;
    }
    if (m_dim_columns_min == m_dim_columns_max)
    {
        m_dim_colunms_count = m_dim_columns_max;
        return m_dim_colunms_count;
    }
    else
    {
        m_dim_colunms_count = -1;
        return -1;
    }
}

void Frame2D::set_data_dim(const int line_num, const int col_num)
{
    m_data.resize(line_num);

//    std::cout << "data<> size is " << m_data.size() << " and should be " << line_num << std::endl;

    for (int i = 0; i < m_data.size(); i++)
    {
        m_data[i].resize(col_num);
//        std::cout << i << ": data<> size is " << m_data.size() << " and should be " << line_num << std::endl;
    }
}

void Frame2D::read_sfrm_header(std::ifstream &sfrm_file)
{
    std::string str;
    char s_buf[0x51];
    //int line_count = 0;

    s_buf[0x50] = 0;
    while (!sfrm_file.eof())
    {
        sfrm_file.read(s_buf, 0x50);  // line length = 0x50
        str.assign(s_buf);
        std::cout << str << std::endl;

        if (str.find("IMG:") != std::string::npos)
        {

            std::cout << " COLs: " << m_dim_columns << "; LINs: " << m_dim_lines << std::endl;

            std::cout << " End of header block was reached" << std::endl;

            return;
        }

        if (str.find(":") != std::string::npos)
        {
            // line contains header tag
            std::stringstream ss(str);
            std::string tag_str, info_str;
            getline( ss, tag_str, ':' ); // token = string before :
            getline( ss, info_str, ':' ); // token = string after :
            //std::cout << ">>" << tag_str << "<<" << std::endl << info_str << std::endl;
            if(tag_str.find("NROWS  ")!=std::string::npos)
            {
                std::stringstream ss_info(info_str);
                int i_tmp;
                ss_info >> i_tmp;
                Frame2D::m_dim_lines = i_tmp;
                std::cout << "NROWS: " << i_tmp << std::endl;
            }
            else if(tag_str.find("NCOLS  ")!=std::string::npos)
            {
                std::stringstream ss_info(info_str);
                int i_tmp;
                ss_info >> i_tmp;
                Frame2D::m_dim_columns = i_tmp;
                std::cout << "NCOLS: " << i_tmp << std::endl;
            }
            else if(tag_str.find("NOVERFL")!=std::string::npos)
            {
                std::stringstream ss_info(info_str);
                int i_tmp1, i_tmp2, i_tmp3;
                ss_info >> i_tmp1 >> i_tmp2 >> i_tmp3;
                Frame2D::m_overflow_flag = i_tmp1;
                Frame2D::m_overflow_byte = i_tmp2;
                Frame2D::m_overflow_word = i_tmp3;
                std::cout << "NOVERFLOWS: 2-byte " << i_tmp2 << ", 4-byte " << i_tmp3 << std::endl;
            }
            else if(tag_str.find("MAXIMUM")!=std::string::npos)
            {
                std::stringstream ss_info(info_str);
                int i_tmp;
                ss_info >> i_tmp;
                Frame2D::m_counts_max = i_tmp;
                std::cout << "MAXIMUM counts: " << i_tmp << std::endl;
            }
/*            else if(tag_str.find("MINIMUM")!=std::string::npos)
            {
                std::stringstream ss_info(info_str);
                int i_tmp;
                ss_info >> i_tmp;
                Frame2D::m_counts_min = i_tmp;
                std::cout << "MINIMUM counts: " << i_tmp << std::endl;
            }
/*            else if(tag_str.find("NCOUNTS")!=std::string::npos)
            {
                std::stringstream ss_info(info_str);
                int i_tmp1, i_tmp2;
                ss_info >> i_tmp1 >>  i_tmp2;
                Frame2D::m_counts_sum = i_tmp1;
                std::cout << "TOTAL counts: " << i_tmp1 << std::endl;
            }
*/        }
    }
}

void Frame2D::read_sfrm_body(std::ifstream& sfrm_file)
{

    std::string str;
    char line_buf[Frame2D::m_dim_columns];
    int byte_overflows = 0;
    int byte_overflow_ln[Frame2D::m_overflow_byte]; // <- number of byte overflows
    int byte_overflow_cn[Frame2D::m_overflow_byte]; // <- number of byte overflows

    // main data block
    for(int line_count = 0; line_count < Frame2D::m_dim_lines; line_count++)
    {
        sfrm_file.read(line_buf, Frame2D::m_dim_columns);
        for(int col_count = 0; col_count < Frame2D::m_dim_columns; col_count++)  // col_count < m_dim_columns;
        {
            int cur_val = line_buf[col_count];
            Frame2D::m_data[line_count][col_count] = (unsigned char) line_buf[col_count];
            //std::cout << cur_val << " ";
            if (cur_val == Frame2D::m_overflow_flag) // != NOVERFLOW[1]
            {
                byte_overflow_ln[byte_overflows] = line_count;
                byte_overflow_cn[byte_overflows] = col_count;
                byte_overflows++;
            }
        }
    }
    std::cout << std::endl;
    std::cout << "Number of 1-byte overflows are " << byte_overflows << std::endl;

    if(byte_overflows != Frame2D::m_overflow_byte)
    {
        std::cout << "Wrong number of 1-byte overflows!" << std::endl;
    }

    // byte overflow data block
    char byte_overflow_buf[byte_overflows * 2];
    int word_overflows = 0;
    int word_overflow_ln[1024]; // <- number of word overflows
    int word_overflow_cn[1024]; // <- number of word overflows

    sfrm_file.read(byte_overflow_buf, byte_overflows * 2);

    for(int overflow_count = 0; overflow_count < byte_overflows; overflow_count++)
    {
        int cur_val = (unsigned char) byte_overflow_buf[overflow_count*2] + 0x100 * (unsigned char) byte_overflow_buf[overflow_count*2 + 1];
        int ln = byte_overflow_ln[overflow_count];
        int cn = byte_overflow_cn[overflow_count];
        Frame2D::m_data[ln][cn] = cur_val;
        if (cur_val == Frame2D::m_overflow_flag)
        {
            word_overflow_ln[word_overflows] = ln;
            word_overflow_cn[word_overflows] = cn;
            word_overflows++;
        }
    }
    std::cout << "Number of 2-byte overflows are " << word_overflows << std::endl;
    if(word_overflows != Frame2D::m_overflow_word)
    {
        std::cout << "Wrong number of 2-byte overflows!" << std::endl;
    }

    char word_overflow_buf[word_overflows];
    int long_overflows = 0;

    sfrm_file.read(word_overflow_buf, word_overflows);

    for(int overflow_count = 0; overflow_count < word_overflows; overflow_count++)
    {
        int cur_val = (unsigned char) word_overflow_buf[overflow_count*4] + 0x100*(unsigned char) word_overflow_buf[overflow_count*4+1] +
                      0x10000*(unsigned char) word_overflow_buf[overflow_count*4+2] + 0x1000000*(unsigned char) word_overflow_buf[overflow_count*4+3];
        int ln = word_overflow_ln[overflow_count];
        int cn = word_overflow_cn[overflow_count];
        Frame2D::m_data[ln][cn] = cur_val;
        if (cur_val == Frame2D::m_overflow_flag) // != NOVERFLOW[1]
        {
            long_overflows++;
        }
    }
    std::cout << "Number of 4-byte overflows are " << long_overflows << std::endl;
}

void Frame2D::read_sfrm(const std::string &fname)
{
    std::ifstream sfrm_file (fname, std::ios::binary);
    if ( !sfrm_file.is_open() )
    {
        std::cout << "Can not open sfrm file " << fname << std::endl;
        return;
    }
    read_sfrm_header(sfrm_file);

    set_data_dim(Frame2D::m_dim_lines, Frame2D::m_dim_columns);

    read_sfrm_body(sfrm_file);

    std::cout << "OK" << std::endl;

    sfrm_file.close();
}

void Frame2D::show_data_rect(int l_min, int c_min, int l_max, int c_max)
{
    if ((l_min > l_max) || (c_min > c_max)){
        std::cout << "Wrong rectangle output parameters in show_data_rect()." << std::endl;
        return;
    }
    if (l_min < 0) l_min = 0;
    if (c_min < 0) c_min = 0;
    if (l_max >= Frame2D::m_dim_lines) l_max = Frame2D::m_dim_lines - 1;
    if (c_max >= Frame2D::m_dim_columns) c_max = Frame2D::m_dim_columns - 1;

    std::cout << std::dec;
    for (int li = l_min; li <= l_max; li++)
    {
        for (int ci = c_min; ci <=c_max; ci++)
        {
            std::cout << std::setw(5) << Frame2D::m_data[li][ci] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::dec;
}
