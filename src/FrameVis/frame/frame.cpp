#include "frame.h"
#include "frame_data.h"

#include <QFileInfo>

Frame::Frame() :
    id(0) {
}

Frame::Frame(const FrameData &data) :
    id(0), data(data) {
}

void Frame::setId(int id) {
    this->id = id;
}

void Frame::setData(const FrameData &data) {
    this->data = data;
}

void Frame::setPixmap(const QPixmap &pixmap) {
    this->pixmap = pixmap;
}

void Frame::setFilename(const QString &filename) {
    this->filename = QFileInfo(filename).fileName();
}

int Frame::getId() const {
    return id;
}

const FrameData &Frame::getData() const {
    return data;
}

const QPixmap& Frame::getPixmap() const {
    return pixmap;
}

QString Frame::getFilename() const {
    return filename;
}

bool Frame::hasData() const {
    return !data.empty();
}

bool Frame::hasPixmap() const {
    return !(pixmap.size().isEmpty());
}

bool Frame::hasValueAt(const QPoint &point) const {
    return hasData() && getData().isIn(point.x(), point.y());
}

QString Frame::strValueAt(const QPoint &point) const {
    auto result = QString("[%1, %2]").arg(point.x()).arg(point.y());
    if (hasValueAt(point)) {
        result += QString(": %1").arg(getData().at(point.x(), point.y()));
    }
    return result;
}

QString Frame::strValueAtFull(const QPoint &point) const {
    return strValueAt(point) + QString(" (frame %1)").arg(getId());
}

QString Frame::getName() const {
    return QString("%1 (frame %2)").arg(getFilename()).arg(getId());
}

double Frame::getMin() const {
    return (hasData() ? getData().getMin() : 0.0);
}

double Frame::getMax() const {
    return (hasData() ? getData().getMax() : 0.0);
}

Pick Frame::addPick(const QPoint &point) {
    Pick pick(point, this);
    picks.addPick(pick);
    return pick;
}

bool Frame::hasPick(const QPoint &point) const {
    return picks.hasPick(point);
}

Pick Frame::removePick(const QPoint &point) {
    Pick pick = picks.getPick(point);
    picks.removePick(point);
    return pick;
}

Pick Frame::changePick(const QPoint &point, const Pick &new_pick) {
    Pick np = new_pick;
    np.setFrame(this);
    picks.changePick(point, np);
    return np;
}

const std::vector<Pick>& Frame::getPicks() const {
    return picks.getPicks();
}

void Frame::clearPicks() {
    picks.clear();
}
