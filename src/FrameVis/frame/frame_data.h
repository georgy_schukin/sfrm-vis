#pragma once

#include <vector>
#include <QPixmap>

class Colorizer;
class Palette;
class Transformation;

class FrameData {
public:
    FrameData();
    FrameData(int width, int height, const std::vector<double> &data);        

    int getWidth() const {
        return width;
    }

    int getHeight() const {
        return height;
    }

    bool isIn(int x, int y) const;

    double at(int x, int y) const;

    double getMin() const;
    double getMax() const;

    bool empty() const;

    QPixmap toPixmap(const Palette &palette, const Transformation &trans) const;

    static FrameData makeRandom(int width, int height, double min, double max);
    static FrameData makeCircle(int width, int height, double min, double max);
    static FrameData makeRibbon(int width, int height, double min, double max);

    static FrameData loadFromFile(const std::string &filename);

private:
    int width;
    int height;
    std::vector<double> data;
};
