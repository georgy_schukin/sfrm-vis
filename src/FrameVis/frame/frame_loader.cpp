#include "frame_loader.h"
#include "frame2d.h"

#include <vector>

FrameData FrameLoader::readSFRM(const std::string &filename) {
    Frame2D frame;
    frame.read_sfrm(filename);
    std::vector<double> data;
    for (int r = 0; r < frame.get_num_of_rows(); r++) {
        for (int c = 0; c < frame.get_num_of_columns(); c++) {
            data.push_back((double)frame.get_data(r, c));
        }
    }
    return FrameData(frame.get_num_of_columns(), frame.get_num_of_rows(), data);
}
