#include "frame_collection.h"
#include "frame_data.h"
#include "color/palette.h"

#include <algorithm>

FrameCollection::FrameCollection() :
    current_frame_index(0) {
}

void FrameCollection::addFrame(const FrameData &data) {
    addFrame(Frame(data));
}

void FrameCollection::addFrame(const FrameData &data, const QString &filename) {
    Frame frame(data);
    frame.setFilename(filename);
    addFrame(frame);
}

void FrameCollection::addFrame(const FrameData &data, const QPixmap &pixmap, const QString &filename) {
    Frame frame(data);
    frame.setFilename(filename);
    frame.setPixmap(pixmap);
    addFrame(frame);
}

void FrameCollection::addFrame(const Frame &frame) {
    auto frame_ptr = std::make_shared<Frame>(frame);
    frame_ptr->setId(static_cast<int>(frames.size() + 1));
    frames.push_back(frame_ptr);
}

Frame* FrameCollection::getFrame(int index) const {
    return (index >= 0 && index < getNumOfFrames()) ? frames[index].get() : nullptr;
}

Frame* FrameCollection::getFrameById(int id) const {
    for (const auto &frame: frames) {
        if (frame->getId() == id) {
            return frame.get();
        }
    }
    return nullptr;
}

int FrameCollection::getFrameIndex(const Frame *frame) const {
    for (int i = 0; i < frames.size(); i++) {
        if (frames[i].get() == frame) {
            return i;
        }
    }
    return -1;
}

Frame *FrameCollection::getCurrentFrame() const {
    return getFrame(current_frame_index);
}

Frame* FrameCollection::nextFrame(const Frame *frame) {
    const auto index = getFrameIndex(frame);
    return (index != -1 ? getFrame((index + 1) % frames.size()) : nullptr);
}

Frame* FrameCollection::prevFrame(const Frame *frame) {
    const auto index = getFrameIndex(frame);
    return (index != -1 ? getFrame((index + frames.size() - 1) % frames.size()) : nullptr);
}

int FrameCollection::getNumOfFrames() const {
    return frames.size();
}

void FrameCollection::clear() {
    frames.clear();
    current_frame_index = 0;
}

void FrameCollection::generatePixmaps(const Palette &palette, const Transformation &trans) {
    for (auto &frame: frames) {
        if (frame->hasData()) {
            frame->setPixmap(frame->getData().toPixmap(palette, trans));
        }
    }
}

double FrameCollection::getMinValue() const {
    std::vector<double> mins;
    for (const auto &frame: frames) {
        mins.push_back(frame->getMin());
    }
    return (!mins.empty() ? *std::min_element(mins.begin(), mins.end()) : 0.0);
}

double FrameCollection::getMaxValue() const {
    std::vector<double> maxs;
    for (const auto &frame: frames) {
        maxs.push_back(frame->getMax());
    }
    return (!maxs.empty() ? *std::max_element(maxs.begin(), maxs.end()) : 0.0);
}
