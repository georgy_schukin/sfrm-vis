#ifndef MAGNIFY_PANE_H
#define MAGNIFY_PANE_H

#include <QWidget>
#include <vector>

namespace Ui {
class MagnifyPane;
}

class Frame;
class MagnifyWidget;

class MagnifyPane : public QWidget {
    Q_OBJECT

public:
    explicit MagnifyPane(QWidget *parent = nullptr);
    ~MagnifyPane() override;

    void clear();
    MagnifyWidget* addMagnifyWidget(int view_size = 200);
    MagnifyWidget* getMagnifyWidget(int index);

    void setFrame(int widget_index, const Frame *frame);

protected:
    void adjustScrollSize();
    void resizeEvent(QResizeEvent *event) override;

private:
    Ui::MagnifyPane *ui;
    std::vector<MagnifyWidget*> magnify_widgets;
    class QLayout *magnify_layout;
};

#endif // MAGNIFY_PANE_H
