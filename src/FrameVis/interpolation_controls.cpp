#include "interpolation_controls.h"
#include "ui_interpolation_controls.h"

#include <cmath>
#include <QHBoxLayout>
#include <QVBoxLayout>

InterpolationControls::InterpolationControls(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::InterpolationControls)
{
    ui->setupUi(this);

    connect(ui->lowSlider, &QSlider::valueChanged, this, &InterpolationControls::setMin);
    connect(ui->lowSpin, static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged),
            this, &InterpolationControls::setMin);
    connect(ui->upSlider, &QSlider::valueChanged, this, &InterpolationControls::setMax);
    connect(ui->upSpin, static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged),
            this, &InterpolationControls::setMax);

    connect(this, &InterpolationControls::minChanged, ui->lowSlider, &QSlider::setValue);
    connect(this, &InterpolationControls::minChanged, ui->lowSpin, &QDoubleSpinBox::setValue);
    connect(this, &InterpolationControls::minChanged, ui->upSlider, &QSlider::setMinimum);
    connect(this, &InterpolationControls::minChanged, ui->upSpin, &QDoubleSpinBox::setMinimum);

    connect(this, &InterpolationControls::maxChanged, ui->upSlider, &QSlider::setValue);
    connect(this, &InterpolationControls::maxChanged, ui->upSpin, &QDoubleSpinBox::setValue);
    connect(this, &InterpolationControls::maxChanged, ui->lowSlider, &QSlider::setMaximum);
    connect(this, &InterpolationControls::maxChanged, ui->lowSpin, &QDoubleSpinBox::setMaximum);

    dynamic_update = ui->dynamicUpdate->isChecked();
    ui->applyButton->setEnabled(!dynamic_update);
}

InterpolationControls::~InterpolationControls() {
    delete ui;
}

void InterpolationControls::setMin(double value) {
    if (std::fabs(current_min - value) > 1e-8) {
        current_min = value;
        emit minChanged(current_min);
        if (dynamic_update) {
            emit interpolationChanged(current_min, current_max);
        }
    }
}

void InterpolationControls::setMax(double value) {
    if (std::fabs(current_max - value) > 1e-8) {
        current_max = value;
        emit maxChanged(current_max);
        if (dynamic_update) {
            emit interpolationChanged(current_min, current_max);
        }
    }
}

void InterpolationControls::setMinMin(double value) {
    ui->lowSlider->setMinimum(static_cast<int>(value));
    ui->lowSpin->setMinimum(value);
}

void InterpolationControls::setMaxMax(double value) {
    ui->upSlider->setMaximum(static_cast<int>(value));
    ui->upSpin->setMaximum(value);
}

void InterpolationControls::setVerticalOrientation(bool vertical) {
    QLayout *new_layout = nullptr;
    if (vertical) {
        new_layout = new QVBoxLayout();
    } else {
        new_layout = new QHBoxLayout();
    }
    if (!new_layout) {
        return;
    }
    auto *old_layout = layout();
    auto children = old_layout->children();
    for (auto *obj: children) {
        auto *item = dynamic_cast<QLayoutItem*>(obj);
        if (item) {
            old_layout->removeItem(item);
            new_layout->addItem(item);
        }
    }
    delete old_layout;
    setLayout(new_layout);
}

void InterpolationControls::enableDynamicControls(bool enable) {
    if (enable) {
        ui->dynamicUpdate->show();
        ui->applyButton->show();
        dynamic_update = ui->dynamicUpdate->isChecked();
    } else {
        ui->dynamicUpdate->hide();
        ui->applyButton->hide();
        dynamic_update = true;
    }
}

void InterpolationControls::on_dynamicUpdate_stateChanged(int arg1) {
    dynamic_update = ui->dynamicUpdate->isChecked();
    ui->applyButton->setEnabled(!dynamic_update);
    if (dynamic_update) {
        emit interpolationChanged(current_min, current_max);
    }
}

void InterpolationControls::on_applyButton_clicked() {
    emit interpolationChanged(current_min, current_max);
}
