#include "main_window.h"
#include <QApplication>
#include <QGuiApplication>
#include <QStyle>
#include <QScreen>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    a.setOrganizationName("Supercomputer Software Department");
    a.setOrganizationDomain("ssd.sscc.ru");
    a.setApplicationName("Frame visualizer");

    MainWindow w;

    // Center and resize the main window.
    const auto geom = QGuiApplication::screens().at(0)->geometry();
    const auto screen_size = geom.size();
    QSize new_size(static_cast<int>(screen_size.width()*0.6f),
                   static_cast<int>(screen_size.height()*0.8f));
    w.setGeometry(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, new_size, geom));

    w.show();

    return a.exec();
}
