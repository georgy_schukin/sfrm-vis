#pragma once

#include <QDialog>

namespace Ui {
class InterpolationSettings;
}

class InterpolationSettings : public QDialog {
    Q_OBJECT

public:
    explicit InterpolationSettings(QWidget *parent, double min, double max, double min_min, double max_max);
    ~InterpolationSettings() override;

    double getCurrentMin() const;
    double getCurrentMax() const;

    void setThisFrameBounds(double min, double max);
    void setAllFramesBounds(double min, double max);

public slots:
    void setMin(double value);
    void setMax(double value);

private slots:
    void on_currentMinAsMin_clicked();
    void on_currentMaxAsMax_clicked();
    void on_allMinAsMin_clicked();
    void on_allMaxAsMax_clicked();

private:
    Ui::InterpolationSettings *ui;

    QSizeF this_frame_bounds {0, 0};
    QSizeF all_frames_bounds {0, 0};
};

