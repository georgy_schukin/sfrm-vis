#include "colorizer.h"

#include <cmath>

namespace {
    const std::vector<QColor> getRainbow(int size) {
        std::vector<QColor> rainbow;        
        for (int step = 0; step < size; step++) {
            rainbow.push_back(QColor::fromHslF(double(step)*0.8/size, 0.95, 0.5));
        }
        rainbow.push_back(Qt::black);
        return rainbow;
    }
}

Colorizer::Colorizer(double min, double max, int size) {
    const auto rainbow = getRainbow(size);
    const auto step = (max - min)/size;
    for (int i = 0; i < size; i++) {
        addColor(min + step*i, rainbow[rainbow.size() - i - 1]);
    }
}

void Colorizer::addColor(const double &value, const QColor &color) {
    colors.insert(ColorPoint(value, color));
}

void Colorizer::setColorValue(int index, const double &value) {
    const auto color = getColor(index);
    removeColor(index);
    addColor(value, color);
}

void Colorizer::removeColor(int index) {
    colors.erase(atIndex(index));
}

QColor Colorizer::getColor(int index) const {
    return atIndex(index)->color;
}

QColor Colorizer::lerp(const double &value) const {
    const auto segment = getSegment(value);
    if (std::fabs(segment.size()) < 1e-6) {
        return segment.begin.color;
    }
    const auto t = (value - segment.begin.value)/segment.size();
    const auto c1 = segment.begin.color;
    const auto c2 = segment.end.color;
    const auto red = (1.0 - t)*c1.red() + t*c2.red();
    const auto green = (1.0 - t)*c1.green() + t*c2.green();
    const auto blue = (1.0 - t)*c1.blue() + t*c2.blue();
    return QColor::fromRgb(red, green, blue);
}

Colorizer::ColorSegment Colorizer::getSegment(const double &value) const {
    const static ColorPoint empty(0.0, QColor(Qt::black));
    if (colors.empty()) {
        return ColorSegment(empty, empty);
    }
    auto it = colors.begin();
    if ((colors.size() == 1) || (value < (*it).value)) {
        return ColorSegment(*it, *it);
    }
    auto next = it;
    next++;
    while (next != colors.end()) {
        if ((value >= (*it).value) && (value <= (*next).value)) {
            return ColorSegment(*it, *next);
        }
        it++;
        next++;
    }
    return ColorSegment(*it, *it);
}

Colorizer::ColorPoints::iterator Colorizer::atIndex(int index) {
    auto it = colors.begin();
    std::advance(it, index);
    return it;
}

Colorizer::ColorPoints::const_iterator Colorizer::atIndex(int index) const {
    auto it = colors.begin();
    std::advance(it, index);
    return it;
}
