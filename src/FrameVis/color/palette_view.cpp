#include "palette_view.h"
#include "palette.h"

#include <QPainter>

PaletteView::PaletteView(QWidget *parent) :
    QWidget(parent) {
}

void PaletteView::setPalette(const Palette *palette) {
    this->palette = palette;
    update();
}

void PaletteView::paintEvent(QPaintEvent *event) {
    QWidget::paintEvent(event);

    QPainter painter(this);
    for (int i = 0; i < width(); i++) {
        painter.fillRect(QRect(i, 0, 1, height()), palette->getColor(double(i)/(width() - 1)));
    }
}
