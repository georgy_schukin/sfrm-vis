#pragma once

#include <QColor>

class Palette {
public:
    virtual ~Palette() = default;

    // Value must be in [0, 1].
    virtual QColor getColor(const double &value) const = 0;
};
