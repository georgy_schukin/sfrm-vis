#include "monochrome_palette.h"

MonochromePalette::MonochromePalette() {
    addColor(QColor(Qt::black));
    addColor(QColor(Qt::white));
}
