#pragma once

#include <set>
#include <QColor>

class Colorizer {
public:
    Colorizer() {}
    Colorizer(double min, double max, int size=7);
    ~Colorizer() {}

    void addColor(const double &value, const QColor &color);
    void setColorValue(int index, const double &value);
    void removeColor(int index);

    QColor getColor(int index) const;

    QColor lerp(const double &value) const;

    int getNumOfColors() const {
        return colors.size();
    }

private:
    struct ColorPoint {
        double value;
        QColor color;

        ColorPoint(const double &value, const QColor color) :
            value(value), color(color) {
        }
    };

    struct ColorSegment {
        ColorPoint begin, end;

        ColorSegment(const ColorPoint &begin, const ColorPoint &end) :
            begin(begin), end(end) {
        }

        double size() const {
            return end.value - begin.value;
        }
    };

    struct ColorPointCmp {
        bool operator()(const ColorPoint &cp1, const ColorPoint cp2) const {
            return cp1.value < cp2.value;
        }
    };
    typedef std::set<ColorPoint, ColorPointCmp> ColorPoints;

private:
    ColorSegment getSegment(const double &value) const;
    ColorPoints::iterator atIndex(int index);
    ColorPoints::const_iterator atIndex(int index) const;

private:    
    ColorPoints colors;
};
