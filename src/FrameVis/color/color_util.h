#pragma once

#include <QColor>

class ColorUtil {
public:
    static QColor lerp(const QColor &c1, const QColor &c2, const double &t);
};
