#include "rainbow_palette.h"
#include "color_util.h"

#include <cmath>

RainbowPalette::RainbowPalette(int num_of_colors, bool add_black) {
    if (add_black) {        
        const auto colors_to_add = num_of_colors/7;
        if (colors_to_add > 0) {
            QColor purple = QColor::fromHslF(0.8, 0.95, 0.5);
            QColor black(Qt::black);
            for (int i = 0; i < colors_to_add; i++) {
                addColor(ColorUtil::lerp(black, purple, double(i)/colors_to_add));
            }
        } else {
            addColor(Qt::black);
        }
    }    
    for (int step = 0; step < num_of_colors; step++) {
        addColor(QColor::fromHslF(double(num_of_colors - step - 1)*0.8/num_of_colors, 0.95, 0.5));
    }
}

