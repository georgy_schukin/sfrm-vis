#pragma once

#include "palette.h"

#include <vector>
#include <initializer_list>

class ColorPalette : public Palette {
public:
    ColorPalette() {}
    ColorPalette(const std::vector<QColor> &colors);

    QColor getColor(const double &value) const override;

    void addColor(const QColor &color);

private:
    std::vector<QColor> colors;
    size_t num_of_steps {0};
    double step {0.0};
};
