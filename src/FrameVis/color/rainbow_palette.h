#pragma once

#include "color_palette.h"

#include <vector>

class RainbowPalette : public ColorPalette {
public:
    RainbowPalette(int num_of_colors=10, bool add_black=false);
};
