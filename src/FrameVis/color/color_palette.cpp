#include "color_palette.h"
#include "color_util.h"

#include <cmath>

ColorPalette::ColorPalette(const std::vector<QColor> &colors) {
    for (const auto &color: colors) {
        addColor(color);
    }
}

void ColorPalette::addColor(const QColor &color) {
    colors.push_back(color);
    num_of_steps = colors.size() - 1;
    step = num_of_steps > 0 ? 1.0/num_of_steps : 1.0;
}

QColor ColorPalette::getColor(const double &value) const {
    const auto start_index = static_cast<size_t>(std::floor(num_of_steps*value));
    const auto end_index = static_cast<size_t>(std::ceil(num_of_steps*value));
    const double t = (value - start_index*step)/step;
    return ColorUtil::lerp(colors[start_index], colors[end_index], t);
}
