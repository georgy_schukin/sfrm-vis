#include "color_util.h"

QColor ColorUtil::lerp(const QColor &c1, const QColor &c2, const double &t) {
    const auto red = (1.0 - t)*c1.redF() + t*c2.redF();
    const auto green = (1.0 - t)*c1.greenF() + t*c2.greenF();
    const auto blue = (1.0 - t)*c1.blueF() + t*c2.blueF();
    return QColor::fromRgbF(red, green, blue);
}
