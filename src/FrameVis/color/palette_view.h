#pragma once

#include <QWidget>

class Palette;

class PaletteView : public QWidget {
    Q_OBJECT
public:
    PaletteView(QWidget *parent = nullptr);

public slots:
    void setPalette(const Palette *palette);

protected:
    void paintEvent(QPaintEvent *event) override;

private:
    const Palette *palette {nullptr};
};
