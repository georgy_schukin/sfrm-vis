#-------------------------------------------------
#
# Project created by QtCreator 2017-04-10T23:59:33
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FrameVis
TEMPLATE = app

DESTDIR = $$PWD

SOURCES += main.cpp\
    color/color_palette.cpp \
    color/monochrome_palette.cpp \
    color/palette_view.cpp \
    interpolation_controls.cpp \
    magnify_pane.cpp \
    main_window.cpp \
    my_graphics_scene.cpp \
    frame/frame_loader.cpp \
    frame/frame2d.cpp \
    my_graphics_view.cpp \
    color/rainbow_palette.cpp \
    color/color_util.cpp \
    frame/frame_collection.cpp \
    clickable_label.cpp \
    color/colorizer.cpp \
    frame/frame_data.cpp \
    frame/frame.cpp \
    pick/pick.cpp \
    pick/pick_collection.cpp \
    pick/pick_dialog.cpp \
    pick/pick_item.cpp \
    pick/pick_settings.cpp \
    transform/linear_interpolation.cpp \
    transform/log_interpolation.cpp \
    transform/square_root_interpolation.cpp \
    transform/interpolation.cpp \
    interpolation_settings.cpp \
    magnify_widget.cpp

HEADERS  += \
    color/color_palette.h \
    color/monochrome_palette.h \
    color/palette_view.h \
    interpolation_controls.h \
    magnify_pane.h \
    main_window.h \
    my_graphics_scene.h \
    frame/frame_loader.h \
    frame/frame2d.h \
    my_graphics_view.h \
    color/palette.h \
    color/rainbow_palette.h \
    color/color_util.h \
    frame/frame_collection.h \
    clickable_label.h \
    color/colorizer.h \
    frame/frame_data.h \
    frame/frame.h \
    pick/pick.h \
    pick/pick_collection.h \
    pick/pick_dialog.h \
    pick/pick_item.h \
    pick/pick_settings.h \
    transform/transformation.h \
    transform/linear_interpolation.h \
    transform/log_interpolation.h \
    transform/square_root_interpolation.h \
    transform/interpolation.h \
    interpolation_settings.h \
    magnify_widget.h

FORMS    += \
    interpolation_controls.ui \
    interpolation_settings.ui \
    magnify_pane.ui \
    magnify_widget.ui \
    main_window.ui \
    pick_dialog.ui \
    pick_settings.ui

RESOURCES += \
    resources.qrc

