#include "main_window.h"
#include "ui_main_window.h"
#include "frame/frame.h"
#include "frame/frame_collection.h"
#include "frame/frame_data.h"
#include "color/colorizer.h"
#include "color/rainbow_palette.h"
#include "color/monochrome_palette.h"
#include "transform/linear_interpolation.h"
#include "transform/log_interpolation.h"
#include "transform/square_root_interpolation.h"
#include "clickable_label.h"
#include "magnify_widget.h"
#include "interpolation_settings.h"
#include "pick/pick_dialog.h"
#include "pick/pick_collection.h"
#include "pick/pick_settings.h"
#include "pick/pick.h"

#include <QImage>
#include <QPixmap>
#include <QFileDialog>
#include <QSettings>
#include <QMessageBox>
#include <QGraphicsPixmapItem>
#include <QActionGroup>
#include <QScrollBar>
#include <QKeyEvent>
#include <cmath>

namespace {
    static const QString LAST_OPEN_DIR = "last_open_dir";
    static const QString LAST_SAVE_DIR = "last_save_dir";

    QString getOption(const QString &key) {
        QSettings settings;
        return settings.value(key).toString();
    }

    void setOption(const QString &key, const QString &value) {
        QSettings settings;
        settings.setValue(key, value);
    }

    class FrameListItem : public QListWidgetItem {
        public:
            FrameListItem(QIcon icon, QString text, Frame *frame) :
                QListWidgetItem(icon, text),
                frame(frame) {}

            Frame* getFrame() const {
                return frame;
            }
        private:
            Frame *frame;
    };

    bool equal(double a, double b) {
        return (std::fabs(a - b) < 1e-8);
    }
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    frames = std::make_shared<FrameCollection>();
    current_palette = std::make_shared<RainbowPalette>(10, true);
    current_interpolation = std::make_shared<LinearInterpolation>(0, 1);

    ui->graphicsView->setScene(&scene);
    ui->graphicsView->setMouseTracking(true);
    //ui->graphicsView->setDragMode(QGraphicsView::ScrollHandDrag);

    frame_list = ui->listWidget;
    frame_list->setDragEnabled(false);
    frame_list->setDragDropMode(QListWidget::NoDragDrop);
    frame_list->hide();

    frame_pane = ui->magnifyPane;
    for (int i = 0; i < 3; i++) {
        auto *widget = frame_pane->addMagnifyWidget(180);
        widget->makeSquareSize(180);
        connect(this, &MainWindow::currentFramePointChanged, widget, &MagnifyWidget::setCurrentPoint);
        //connect(ui->graphicsView, &MyGraphicsView::zoomChanged, widget, &MagnifyWidget::setZoom);
    }
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            connect(frame_pane->getMagnifyWidget(i), &MagnifyWidget::zoomChanged,
                    frame_pane->getMagnifyWidget(j), &MagnifyWidget::setZoom);
        }
    }

    ui->splitter->setStretchFactor(0, 2);
    ui->splitter->setStretchFactor(1, 0);
    ui->splitter->setSizes({180, 180});

    connect(ui->graphicsView, &MyGraphicsView::sceneMouseOver, this, &MainWindow::setCurrentFramePoint);
    connect(ui->graphicsView, &MyGraphicsView::sceneMousePressed, this, &MainWindow::onSceneMousePressed);

    connect(&scene, &MyGraphicsScene::pickRemoved, this, &MainWindow::removePick);
    connect(&scene, &MyGraphicsScene::pickChanged, this, &MainWindow::changePick);
    connect(&scene, &MyGraphicsScene::pickClicked, this, &MainWindow::onPickClicked);

    connect(this, &MainWindow::pickAdded, [this](const Pick &pick) {
        this->scene.addPick(pick);
    });
    connect(this, &MainWindow::pickChanged, &scene, &MyGraphicsScene::changePick);
    connect(this, &MainWindow::pickRemoved, &scene, &MyGraphicsScene::removePick);
    connect(this, &MainWindow::picksCleared, &scene, &MyGraphicsScene::clearPicks);

    connect(this, &MainWindow::framesAdded, this, &MainWindow::onLoadedFrames);

    connect(this, &MainWindow::currentFramePointChanged, this, &MainWindow::updateStatus);
    //connect(this, &MainWindow::frameCursorUnfocused, this, &MainWindow::clearStatus);

    connect(this, &MainWindow::interpolationChanged, this, &MainWindow::updatePixmaps);
    connect(this, &MainWindow::interpolationChanged, [this](const Interpolation *interp){
        this->ui->interpControls->setMin(interp->getMin());
        this->ui->interpControls->setMax(interp->getMax());
    });
    connect(ui->interpControls, &InterpolationControls::interpolationChanged, this, &MainWindow::setInterpolationMinMax);

    connect(this, &MainWindow::paletteChanged, this, &MainWindow::updatePixmaps);

    connect(this, &MainWindow::framesUpdated, this, &MainWindow::updateJumbotron);
    connect(this, &MainWindow::framesUpdated, [this](){
        this->updateFrameView(this->getCurrentFrame());
    });
    connect(this, &MainWindow::framesUpdated, [this](){
        this->updateMagnifyPane(this->getCurrentFrame());
    });

    connect(frame_list, &QListWidget::currentItemChanged, [this](QListWidgetItem *curr, QListWidgetItem *prev) {
        auto *curr_frame_item = dynamic_cast<FrameListItem*>(curr);
        if (!curr_frame_item) {
            this->setCurrentFrame(nullptr);
        } else {
            this->setCurrentFrame(curr_frame_item->getFrame());
        }
    });

    connect(this, &MainWindow::currentFrameChanged, this, &MainWindow::updateWindowTitle);
    connect(this, &MainWindow::currentFrameChanged, [this](const Frame *frame) {
        this->updateFrameView(frame);
    });
    connect(this, &MainWindow::currentFrameChanged, [this](const Frame *frame) {
        emit this->currentFramePointChanged(this->getCurrentFramePoint());
    });
    connect(this, &MainWindow::currentFrameChanged, this, &MainWindow::updateMagnifyPane);

    auto *interp_group = new QActionGroup(this);
    interp_group->addAction(ui->actionLinear);
    interp_group->addAction(ui->actionLogarithmic);
    interp_group->addAction(ui->actionSquare_root);
    ui->actionLinear->setChecked(true);

    auto *palette_group = new QActionGroup(this);
    palette_group->addAction(ui->actionPalette_Rainbow);
    palette_group->addAction(ui->actionPalette_Monochrome);
    palette_group->addAction(ui->actionPalette_Gnuplot);
    palette_group->addAction(ui->actionPalette_Rainbow_without_Black);
    ui->actionPalette_Rainbow->setChecked(true);

    auto *mouse_mode_group = new QActionGroup(this);
    mouse_mode_group->addAction(ui->actionMouseModeDrag);
    mouse_mode_group->addAction(ui->actionMouseModeAdd_Pick);
    mouse_mode_group->addAction(ui->actionMouseModeDelete_Pick);

    ui->mainToolBar->addAction(ui->actionMouseModeDrag);
    ui->mainToolBar->addAction(ui->actionMouseModeAdd_Pick);
    ui->mainToolBar->addAction(ui->actionMouseModeDelete_Pick);
    ui->actionMouseModeDrag->setChecked(true);

    connect(this, &MainWindow::mouseModeChanged, [this](MouseMode mode) {
       this->ui->graphicsView->enableDrag(mode == MouseMode::Drag);
       this->ui->actionMouseModeDrag->setChecked(mode == MouseMode::Drag);
       this->ui->actionMouseModeAdd_Pick->setChecked(mode == MouseMode::AddPick);
       this->ui->actionMouseModeDelete_Pick->setChecked(mode == MouseMode::DeletePick);
    });

    setMouseMode(MouseMode::Drag);

    ui->paletteView->setFixedHeight(15);
    ui->paletteView->setPalette(current_palette.get());
    connect(this, &MainWindow::paletteChanged, ui->paletteView, &PaletteView::setPalette);

    adjustSize();
    updateGeometry();
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::loadFramesFromFiles(const QStringList &filenames) {
    clearFrames();
    addFramesFromFiles(filenames);
}

void MainWindow::addFramesFromFiles(const QStringList &filenames) {
    for (const auto &filename: filenames) {
        addFrameFromFile(filename);
    }
    emit framesAdded();
}

void MainWindow::addFrameFromFile(const QString &filename) {
    if (filename.contains(".sfrm")) {
        addFrame(FrameData::loadFromFile(filename.toStdString()), filename);
    } else {
        addPixmapOnlyFrame(QPixmap(filename), filename);
    }
}

void MainWindow::setSingleFrame(const FrameData &frame) {
    clearFrames();
    addFrame(frame);
    emit framesAdded();
}

void MainWindow::addFrame(const FrameData &frame, const QString &filename) {
    frames->addFrame(frame, filename);
}

void MainWindow::addPixmapOnlyFrame(const QPixmap &pixmap, const QString &filename) {
    frames->addFrame(FrameData(), pixmap, filename);
}

void MainWindow::clearFrames() {
    frames->clear();
    emit picksCleared();
    emit picksUpdated();
}

void MainWindow::setCurrentFrame(Frame *frame) {
    if (frame != current_frame) {
        current_frame = frame;
        emit currentFrameChanged(frame);
    }
}

Frame* MainWindow::getCurrentFrame() const {
    return current_frame;
}

void MainWindow::setCurrentPixmap(const QPixmap &pixmap) {
    scene.setPixmap(pixmap);
    ui->graphicsView->setSceneRect(scene.getPixmapRect());
    //ui->graphicsView->centerOn(current_pixmap);
}

void MainWindow::fitPixmapToScreen() {
    if (getCurrentFrame()) {
        ui->graphicsView->fitInView(getCurrentFrame()->getPixmap().rect(), Qt::KeepAspectRatio);
    }
}

void MainWindow::addPick(const QPoint &point) {
    auto *current = getCurrentFrame();
    if (!current) {
        return;
    }
    if (!current->hasPick(point)) {
        auto pick = current->addPick(point);
        emit pickAdded(pick);
        emit picksUpdated();
    }
}

void MainWindow::changePick(const Pick &pick, const Pick &new_pick) {
    auto *frame = frames->getFrameById(pick.getFrame()->getId());
    if (!frame) {
        return;
    }
    if (frame->hasPick(pick.getPoint())) {
        auto updated_pick = frame->changePick(pick.getPoint(), new_pick);
        emit pickChanged(pick, updated_pick);
        emit picksUpdated();
    }
}

void MainWindow::removePick(const Pick &pick) {
    auto *frame = frames->getFrameById(pick.getFrame()->getId());
    if (!frame) {
        return;
    }
    if (frame->hasPick(pick.getPoint())) {
        frame->removePick(pick.getPoint());
        emit pickRemoved(pick);
        emit picksUpdated();
    }
}

std::vector<Pick> MainWindow::getAllPicks() const {
    std::vector<Pick> result;
    for (int i = 0; i < frames->getNumOfFrames(); i++) {
        const auto &picks = frames->getFrame(i)->getPicks();
        result.insert(result.end(), picks.begin(), picks.end());
    }
    return result;
}

void MainWindow::clearCurrentPicks() {
    auto *current = getCurrentFrame();
    if (!current || current->getPicks().empty()) {
        return;
    }
    const auto answer = QMessageBox::question(this,
                                              "Delete picks",
                                              QString("Delete all picks for frame %1?").arg(current->getId()));
    if (answer == QMessageBox::Yes) {
        current->clearPicks();
        emit picksCleared();
        emit picksUpdated();
    }
}

void MainWindow::clearAllPicks() {
    const auto answer = QMessageBox::question(this,
                                              "Delete picks",
                                              QString("Delete all picks for all frames?"));
    if (answer == QMessageBox::Yes) {
        for (int i = 0; i < frames->getNumOfFrames(); i++) {
            frames->getFrame(i)->clearPicks();
        }
        emit picksCleared();
        emit picksUpdated();
    }
}

void MainWindow::centerOnPick(const Pick &pick) {
    auto *frame = frames->getFrameById(pick.getFrame()->getId());
    if (frame) {
        setCurrentFrame(frame);
        ui->graphicsView->centerOnPick(pick);
        setCurrentFramePoint(pick.getPoint());
    }
}

void MainWindow::setMouseMode(MouseMode mode) {
    mouse_mode = mode;
    emit mouseModeChanged(mode);
}

void MainWindow::onSceneMousePressed(const QPoint &point) {
    if (mouse_mode == MouseMode::AddPick) {
        addPick(point);
    }
}

void MainWindow::onPickClicked(const Pick &pick) {
    if (mouse_mode == MouseMode::DeletePick) {
        removePick(pick);
    }
}

void MainWindow::on_actionOpen_triggered() {
    auto filenames = getOpenFileNames();
    if (!filenames.isEmpty()) {
        filenames.sort();
        loadFramesFromFiles(filenames);
        setOption(LAST_OPEN_DIR, QDir().absoluteFilePath(filenames.at(0)));
    }
}

void MainWindow::on_actionAdd_triggered() {
    auto filenames = getOpenFileNames();
    if (!filenames.isEmpty()) {
        filenames.sort();
        addFramesFromFiles(filenames);
        setOption(LAST_OPEN_DIR, QDir().absoluteFilePath(filenames.at(0)));
    }
}

QStringList MainWindow::getOpenFileNames() {
    const QString name_filter = "SFRM files (*.sfrm);; All files (*.*)";
    const QString last_dir = getOption(LAST_OPEN_DIR);
    return QFileDialog::getOpenFileNames(this, "Open file(s)", last_dir, name_filter);
}

void MainWindow::on_actionSave_triggered() {
    const QString name_filter = "PNG files (*.png);;JPEG files (*.jpg *.jpeg);;BMP files (*.bmp);;All files (*.*)";
    const QString last_dir = getOption(LAST_SAVE_DIR);
    QString filename = QFileDialog::getSaveFileName(this, "Save image", last_dir, name_filter);
    if (!filename.isNull()) {
        saveFrameToFile(filename);
        setOption(LAST_SAVE_DIR, QDir().absoluteFilePath(filename));
    }
}

void MainWindow::saveFrameToFile(const QString &filename) {
    if (!getCurrentFrame() || getCurrentFrame()->getPixmap().isNull()) {
        QMessageBox::critical(this, "Error!", "No image to save");
        return;
    }
    getCurrentFrame()->getPixmap().save(filename);
}

void MainWindow::on_actionGenerate_frame_triggered() {
    setSingleFrame(FrameData::makeRandom(800, 800, 0.0, 100.0));
}

void MainWindow::on_actionGenerate_circle_frame_triggered() {
    setSingleFrame(FrameData::makeCircle(800, 800, 0.0, 100.0));
}

void MainWindow::on_actionGenerate_ribbon_frame_triggered() {
    setSingleFrame(FrameData::makeRibbon(1024, 768, 0.0, 100.0));
}

void MainWindow::on_actionAbout_triggered() {
    QMessageBox::information(this, "About", QString("Frame Visualizer v1.2<br/>") +
                             QString("Programmed by Georgy Schukin<br/>") +
                             QString("<a href='mailto:schukin@ssd.sscc.ru'>schukin@ssd.sscc.ru</a>"));
}

void MainWindow::on_actionExit_triggered() {
    qApp->exit();
}

void MainWindow::updateStatus(const QPoint &point) {
    auto *frame = getCurrentFrame();
    if (!frame) {
        return;
    }
    if (frame->hasValueAt(point)) {
        statusBar()->showMessage(frame->strValueAtFull(point));
    } else {
        statusBar()->showMessage("");
    }
}

void MainWindow::clearStatus() {
    statusBar()->showMessage("");
}

void MainWindow::onLoadedFrames() {
    initJumbotron();
    initInterpolation();
    //selectFrame(0);
    fitPixmapToScreen();
}

void MainWindow::initInterpolation() {
    ui->interpControls->setMinMin(frames->getMinValue());
    ui->interpControls->setMaxMax(frames->getMaxValue());
    setInterpolationMinMax(frames->getMinValue(), frames->getMaxValue());
}

void MainWindow::initJumbotron() {
    auto current = frame_list->currentRow();
    frame_list->clear();
    QIcon stub_icon(QPixmap(64, 64)); // use stub icon to prevent list view bug not showing icons
    for (int i = 0; i < frames->getNumOfFrames(); i++) {
        auto *frame = frames->getFrame(i);
        auto *item = new FrameListItem(stub_icon, frame->getFilename(), frame);
        frame_list->addItem(item);
    }
    if (frames->getNumOfFrames() > 1) {
        ui->listWidget->show();
    } else {
        ui->listWidget->hide();
    }
    frame_list->setCurrentRow(std::max(0, std::min(current, frame_list->count() - 1)));
    updateGeometry();
}

void MainWindow::updateJumbotron() {
    for (int i = 0; i < frame_list->count(); i++) {
        auto *item = frame_list->item(i);
        auto *frame = dynamic_cast<FrameListItem*>(item)->getFrame();
        item->setIcon(QIcon(frame->getPixmap()));
    }
}

void MainWindow::on_actionNext_frame_triggered() {
    if (!ui->listWidget->count()) {
        return;
    }
    selectFrame((ui->listWidget->currentRow() + 1) % ui->listWidget->count());
}

void MainWindow::on_actionPrevious_frame_triggered() {
    if (!ui->listWidget->count()) {
        return;
    }
    selectFrame((ui->listWidget->currentRow() + ui->listWidget->count() - 1) % ui->listWidget->count());
}

void MainWindow::selectFrame(int index) {
    ui->listWidget->setCurrentRow(index);
}

void MainWindow::on_actionZoom_in_triggered() {
    ui->graphicsView->zoomIn();
}

void MainWindow::on_actionZoom_out_triggered() {
    ui->graphicsView->zoomOut();
}

void MainWindow::on_actionDiscard_zoom_triggered() {
    ui->graphicsView->discardZoom();
}

void MainWindow::on_actionLinear_triggered() {
    setInterpolation(std::make_shared<LinearInterpolation>(current_interpolation->getMin(), current_interpolation->getMax()));
}

void MainWindow::on_actionLogarithmic_triggered() {
    setInterpolation(std::make_shared<LogInterpolation>(current_interpolation->getMin(), current_interpolation->getMax()));
}

void MainWindow::on_actionSquare_root_triggered() {
    setInterpolation(std::make_shared<SquareRootInterpolation>(current_interpolation->getMin(), current_interpolation->getMax()));
}

void MainWindow::setInterpolation(const std::shared_ptr<Interpolation> &interpolation) {
    if (current_interpolation != interpolation) {
        current_interpolation = interpolation;
        emit interpolationChanged(current_interpolation.get());
    }
}

void MainWindow::setInterpolationMin(double min) {
    const auto current_min = current_interpolation->getMin();
    if (!equal(min, current_min)) {
        current_interpolation->setMin(min);
        emit interpolationChanged(current_interpolation.get());
    }
}

void MainWindow::setInterpolationMax(double max) {
    const auto current_max = current_interpolation->getMax();
    if (!equal(max, current_max)) {
        current_interpolation->setMax(max);
        emit interpolationChanged(current_interpolation.get());
    }
}

void MainWindow::setInterpolationMinMax(double min, double max) {
    const auto current_min = current_interpolation->getMin();
    const auto current_max = current_interpolation->getMax();
    if (true /*!equal(min, current_min) || !equal(max, current_max)*/) {
        current_interpolation->update(min, max);
        emit interpolationChanged(current_interpolation.get());
    }
}

void MainWindow::updatePixmaps() {
    frames->generatePixmaps(*current_palette.get(), *current_interpolation.get());
    emit framesUpdated();
}

void MainWindow::updateFrameView(const Frame *frame) {
    clearView();
    if (!frame) {
        return;
    }
    if (frame->hasPixmap()) {
        setCurrentPixmap(frame->getPixmap());
    }
    for (const auto &pick: frame->getPicks()) {
        scene.addPick(pick);
    }
}

void MainWindow::clearView() {
    scene.clearPixmap();
    scene.clearPicks();
}

void MainWindow::updateWindowTitle(const Frame *frame) {
    QString title("Frame Visualizer");
    if (frame) {
        title += QString(": %1 (min: %2, max: %3)")
                    .arg(frame->getName())
                    .arg(frame->getMin())
                    .arg(frame->getMax());
    }
    setWindowTitle(title);
}

void MainWindow::on_actionSettings_triggered() {
    const auto min = current_interpolation->getMin();
    const auto max = current_interpolation->getMax();
    const auto min_min = frames->getMinValue();
    const auto max_max = frames->getMaxValue();

    InterpolationSettings settings(this, min, max, min_min, max_max);

    const auto *current = getCurrentFrame();
    if (current) {
        settings.setThisFrameBounds(current->getMin(), current->getMax());
    }
    settings.setAllFramesBounds(frames->getMinValue(), frames->getMaxValue());

    if (settings.exec() == QDialog::Accepted) {
        setInterpolationMinMax(settings.getCurrentMin(), settings.getCurrentMax());
    }
}

void MainWindow::setCurrentFramePoint(const QPoint &point) {
    if (point != current_point) {
        current_point = point;
        emit currentFramePointChanged(point);
    }
}

QPoint MainWindow::getCurrentFramePoint() const {
    return current_point;
}

void MainWindow::updateMagnifyPane(const Frame *frame) {
    frame_pane->setFrame(0, frames->prevFrame(frame));
    frame_pane->setFrame(1, frame);
    frame_pane->setFrame(2, frames->nextFrame(frame));
}

void MainWindow::keyPressEvent(QKeyEvent *event) {
    if (event->key() == Qt::Key_Space) {
        setMouseMode(MouseMode::AddPick);
    }
    QMainWindow::keyPressEvent(event);
}

void MainWindow::keyReleaseEvent(QKeyEvent *event) {
    if (event->key() == Qt::Key_Space) {
        setMouseMode(MouseMode::Drag);
    }
    QMainWindow::keyReleaseEvent(event);
}

void MainWindow::on_actionDelete_Current_Picks_triggered() {
    clearCurrentPicks();
}

void MainWindow::on_actionDelete_All_Picks_triggered() {
    clearAllPicks();
}

void MainWindow::on_actionView_Picks_triggered(){
    auto *dlg = new PickDialog(this);

    dlg->setFrameCollection(frames.get());

    connect(this, &MainWindow::picksUpdated, dlg, &PickDialog::updatePicks);
    connect(dlg, &PickDialog::pickChanged, this, &MainWindow::changePick);
    connect(dlg, &PickDialog::pickRemoved, this, &MainWindow::removePick);
    connect(dlg, &PickDialog::pickClicked, this, &MainWindow::centerOnPick);

    //dlg->setAttribute(Qt::WA_ShowWithoutActivating);
    dlg->setAttribute(Qt::WA_DeleteOnClose);

    dlg->show();
}

void MainWindow::on_actionMagnification_Zoom_In_triggered() {
    frame_pane->getMagnifyWidget(0)->zoomIn();
}

void MainWindow::on_actionMagnification_Zoom_Out_triggered() {
    frame_pane->getMagnifyWidget(0)->zoomOut();
}

void MainWindow::on_actionMagnification_Reset_Zoom_triggered() {
    frame_pane->getMagnifyWidget(0)->resetZoom();
}

void MainWindow::on_actionNew_Pick_triggered() {
    PickSettings dlg(Pick(QPoint(0, 0)), this);
    dlg.setWindowTitle("Add new pick");
    auto *current = getCurrentFrame();
    if (current && current->hasPixmap()) {
        dlg.setBounds(current->getPixmap().rect());
    }
    if (dlg.exec() == QDialog::Accepted) {
        addPick(dlg.getPick().getPoint());
    }
}

void MainWindow::on_actionPalette_Rainbow_triggered() {
    setColorPalette(std::make_shared<RainbowPalette>(10, true));
}

void MainWindow::on_actionPalette_Rainbow_without_Black_triggered() {
    setColorPalette(std::make_shared<RainbowPalette>(10, false));
}

void MainWindow::on_actionPalette_Monochrome_triggered() {
    setColorPalette(std::make_shared<MonochromePalette>());
}

void MainWindow::on_actionPalette_Gnuplot_triggered() {
    const std::vector<QColor> colors = {QColor(Qt::black),
                                        QColor(Qt::blue),
                                        QColor(Qt::red),
                                        QColor(Qt::yellow)};
    setColorPalette(std::make_shared<ColorPalette>(colors));
}

void MainWindow::setColorPalette(const std::shared_ptr<Palette> &palette) {
    if (current_palette != palette) {
        current_palette = palette;
        emit paletteChanged(current_palette.get());
    }
}

void MainWindow::on_actionMouseModeDrag_triggered() {
    setMouseMode(MouseMode::Drag);
}

void MainWindow::on_actionMouseModeAdd_Pick_triggered() {
    setMouseMode(MouseMode::AddPick);
}

void MainWindow::on_actionMouseModeDelete_Pick_triggered() {
    setMouseMode(MouseMode::DeletePick);
}

