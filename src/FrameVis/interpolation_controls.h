#pragma once

#include <QWidget>

namespace Ui {
class InterpolationControls;
}

class InterpolationControls : public QWidget {
    Q_OBJECT

public:
    explicit InterpolationControls(QWidget *parent = nullptr);
    ~InterpolationControls() override;

    double getCurrentMin() const {
        return current_min;
    }

    double getCurrentMax() const {
        return current_max;
    }

signals:
    void minChanged(double value);
    void maxChanged(double value);
    void interpolationChanged(double min, double max);

public slots:
    void setMinMin(double value);
    void setMaxMax(double value);
    void setMin(double value);
    void setMax(double value);
    void setVerticalOrientation(bool vertical = true);
    void enableDynamicControls(bool enable = true);

private slots:
    void on_dynamicUpdate_stateChanged(int arg1);

    void on_applyButton_clicked();

private:
    Ui::InterpolationControls *ui;
    double current_min {0.0}, current_max {0.0};
    bool dynamic_update {true};
};

