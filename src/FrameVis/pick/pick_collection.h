#pragma once

#include "pick.h"

#include <vector>

class Frame;

class PickCollection {
public:
    PickCollection() {}

    void addPick(const Pick &pick);
    void addPick(const QPoint &point, const Frame *frame = nullptr);

    bool hasPick(const Pick &pick) const;
    bool hasPick(const QPoint &point) const;

    const std::vector<Pick>& getPicks() const;

    int getPickIndex(const Pick &pick) const;
    int getPickIndex(const QPoint &point) const;

    Pick getPick(const QPoint &point) const;
    Pick getPick(int index) const;

    void changePick(const Pick &pick, const Pick &new_pick);
    void changePick(const QPoint &point, const Pick &new_pick);

    void removePick(const Pick &pick);
    void removePick(const QPoint &point);
    void removePick(int index);

    void clear();

private:
    std::vector<Pick> picks;
};
