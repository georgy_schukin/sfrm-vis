#pragma once

#include <QPoint>
#include <QString>

class Frame;

class Pick {
public:
    Pick() {}
    explicit Pick(const QPoint &point, const Frame *frame = nullptr) :
        point(point), frame(frame) {
    }

    void setPoint(const QPoint &point) {
        this->point = point;
    }

    QPoint getPoint() const {
        return point;
    }

    QString toString() const;

    bool operator==(const Pick &pick) const;
    bool operator==(const QPoint &point) const;

    void setFrame(const Frame *frame) {
        this->frame = frame;
    }

    const Frame* getFrame() const {
        return frame;
    }

private:
    QPoint point {0, 0};
    const Frame *frame {nullptr};
};
