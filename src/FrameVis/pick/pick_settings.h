#pragma once

#include "pick.h"

#include <QDialog>

namespace Ui {
class PickSettings;
}

class PickSettings : public QDialog {
    Q_OBJECT

public:
    explicit PickSettings(const Pick &pick, QWidget *parent = nullptr);
    ~PickSettings();

    void setXBounds(int min_x, int max_x);
    void setYBounds(int min_y, int max_y);
    void setBounds(const QRect &rect);
    void setBounds(const QRectF &rect);

    int getX() const;
    int getY() const;

    Pick getPick() const;

private:
    Ui::PickSettings *ui;
};

