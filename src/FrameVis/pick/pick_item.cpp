#include "pick_item.h"

#include <QPen>
#include <QMenu>
#include <QGraphicsSceneContextMenuEvent>

PickItem::PickItem(const Pick &pick, int item_size, QObject *parent) :
    QObject(parent),
    pick(pick)
{
    setRect(-item_size/2, -item_size/2, item_size, item_size);
    setPos(pick.getPoint().x() + 0.5, pick.getPoint().y() + 0.5);
    //setRect(pick.getPoint().x() - 10, pick.getPoint().y() - 10, 20, 20);
    setPen(QPen(Qt::red));
    setToolTip(pick.toString());
    setAcceptHoverEvents(true);
    //setFlag(QGraphicsItem::ItemIgnoresTransformations);
}

void PickItem::contextMenuEvent(QGraphicsSceneContextMenuEvent *event) {
    /*QMenu menu;
    menu.addAction("Test");
    menu.exec(event->screenPos());*/
    emit contextMenu(event->screenPos(), pick);
    QGraphicsEllipseItem::contextMenuEvent(event);
}

void PickItem::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    emit itemPressed(pick);
    QGraphicsEllipseItem::mousePressEvent(event);
}
