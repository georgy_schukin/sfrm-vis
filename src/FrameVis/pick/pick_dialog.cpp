#include "pick_dialog.h"
#include "ui_pick_dialog.h"
#include "pick.h"
#include "pick_settings.h"
#include "frame/frame.h"
#include "frame/frame_collection.h"

#include <QListWidgetItem>
#include <QPixmap>
#include <QMenu>
#include <QKeyEvent>

PickDialog::PickDialog(QWidget *parent) :
    QDialog(parent, Qt::Window),
    ui(new Ui::PickDialog)
{
    ui->setupUi(this);

    ui->pickList->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->pickList, &QListWidget::customContextMenuRequested, this, &PickDialog::showContextMenu);

    connect(ui->pickList, &QListWidget::itemDoubleClicked, [this](QListWidgetItem *item) {
       auto *pick_item = dynamic_cast<PickListItem*>(item);
       emit this->pickClicked(pick_item->getPick());
    });

    settings_ac = new QAction("Settings...", this);
    delete_ac = new QAction("Delete", this);
    connect(settings_ac, &QAction::triggered, this, &PickDialog::onPickSettings);
    connect(delete_ac, &QAction::triggered, this, &PickDialog::onPickDelete);
}

PickDialog::~PickDialog() {
    delete ui;
}

void PickDialog::setFrameCollection(const FrameCollection *frames) {
    this->frames = frames;
    updatePicks();
}

void PickDialog::updatePicks() {
    updatePicksList();
}

void PickDialog::updatePicksList() {
    auto current = ui->pickList->currentRow();
    ui->pickList->clear();
    if (!frames) {
        return;
    }
    for (int i = 0; i < frames->getNumOfFrames(); i++) {
        const auto *frame = frames->getFrame(i);
        int index = 1;
        for (const auto &pick: frame->getPicks()) {
            auto text = QString("Frame %1: Pick %2: %3")
                    .arg(frame->getId())
                    .arg(index++)
                    .arg(frame->strValueAt(pick.getPoint()));
            auto *item = new PickListItem(pick, text);
            ui->pickList->addItem(item);
        }
    }
    ui->pickList->setCurrentRow(std::max(0, std::min(ui->pickList->count() - 1, current)));
}

const PickDialog::PickListItem *PickDialog::currentItem() const {
    const auto *current = ui->pickList->currentItem();
    return dynamic_cast<const PickListItem*>(current);
}

void PickDialog::onPickSettings() {
    const auto *current = currentItem();
    if (!current) {
        return;
    }
    PickSettings dlg(current->getPick(), nullptr);
    if (frames) {
        const auto *frame = frames->getFrame(0);
        if (frame->hasPixmap()) {
            dlg.setBounds(frame->getPixmap().rect());
        }
    }
    if (dlg.exec() == QDialog::Accepted) {
        emit pickChanged(current->getPick(), dlg.getPick());
    }
}

void PickDialog::onPickDelete() {
    const auto *current = currentItem();
    if (!current) {
        return;
    }
    emit pickRemoved(current->getPick());
}

void PickDialog::showContextMenu(const QPoint &pos) {
    QMenu menu;
    menu.addAction(settings_ac);
    menu.addAction(delete_ac);
    menu.exec(ui->pickList->mapToGlobal(pos));
}

void PickDialog::keyPressEvent(QKeyEvent *event) {
    if (event->key() == Qt::Key_Delete) {
        onPickDelete();
    }
    QDialog::keyPressEvent(event);
}
