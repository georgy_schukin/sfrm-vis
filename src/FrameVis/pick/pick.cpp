#include "pick.h"

QString Pick::toString() const {
    return QString("[%0, %1]").arg(point.x()).arg(point.y());
}

bool Pick::operator==(const Pick &pick) const {
    return (point == pick.getPoint());
}

bool Pick::operator==(const QPoint &point) const {
    return (this->point == point);
}
