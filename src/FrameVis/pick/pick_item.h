#pragma once

#include "pick.h"

#include <QGraphicsEllipseItem>

class PickItem : public QObject, public QGraphicsEllipseItem {
    Q_OBJECT

public:
    PickItem(const Pick &pick, int item_size = 20, QObject *parent = nullptr);

    const Pick& getPick() const {
        return pick;
    }

signals:
    void contextMenu(const QPoint &pos, const Pick &pick);
    void itemPressed(const Pick &pick);

protected:
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event) override;
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;

private:
    Pick pick;
};
