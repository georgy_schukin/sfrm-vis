#include "pick_settings.h"
#include "ui_pick_settings.h"

PickSettings::PickSettings(const Pick &pick, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PickSettings)
{
    ui->setupUi(this);
    ui->pickX->setValue(pick.getPoint().x());
    ui->pickY->setValue(pick.getPoint().y());
}

PickSettings::~PickSettings()
{
    delete ui;
}

void PickSettings::setXBounds(int min_x, int max_x) {
    ui->pickX->setMinimum(min_x);
    ui->pickX->setMaximum(max_x);
}

void PickSettings::setYBounds(int min_y, int max_y) {
    ui->pickY->setMinimum(min_y);
    ui->pickY->setMaximum(max_y);
}

void PickSettings::setBounds(const QRect &rect) {
    setXBounds(0, rect.width());
    setYBounds(0, rect.height());
}

void PickSettings::setBounds(const QRectF &rect) {
    setXBounds(0, static_cast<int>(rect.width()));
    setYBounds(0, static_cast<int>(rect.height()));
}

int PickSettings::getX() const {
    return ui->pickX->value();
}

int PickSettings::getY() const {
    return ui->pickY->value();
}

Pick PickSettings::getPick() const {
    return Pick(QPoint(getX(), getY()));
}
