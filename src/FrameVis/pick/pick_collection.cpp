#include "pick_collection.h"

void PickCollection::addPick(const Pick &pick) {
    picks.push_back(pick);
}

void PickCollection::addPick(const QPoint &point, const Frame *frame) {
    addPick(Pick(point, frame));
}

bool PickCollection::hasPick(const Pick &pick) const {
    return hasPick(pick.getPoint());
}

bool PickCollection::hasPick(const QPoint &point) const {
    for (const auto &pick: picks) {
        if (pick == point) {
            return true;
        }
    }
    return false;
}

const std::vector<Pick>& PickCollection::getPicks() const {
    return picks;
}

int PickCollection::getPickIndex(const Pick &pick) const {
    return getPickIndex(pick.getPoint());
}

int PickCollection::getPickIndex(const QPoint &point) const {
    for (size_t i = 0; i < picks.size(); i++) {
        if (picks[i] == point) {
            return static_cast<int>(i);
        }
    }
    return -1;
}

Pick PickCollection::getPick(const QPoint &point) const {
    return getPick(getPickIndex(point));
}

Pick PickCollection::getPick(int index) const {
    if (index >= 0 && index < static_cast<int>(picks.size())) {
        return picks[static_cast<size_t>(index)];
    }
    return Pick();
}

void PickCollection::changePick(const Pick &pick, const Pick &new_pick) {
    changePick(pick.getPoint(), new_pick);
}

void PickCollection::changePick(const QPoint &point, const Pick &new_pick) {
    const auto index = getPickIndex(point);
    if (index != -1) {
        picks[index] = new_pick;
    }
}

void PickCollection::removePick(const Pick &pick) {
    removePick(getPickIndex(pick));
}

void PickCollection::removePick(const QPoint &point) {
    removePick(getPickIndex(point));
}

void PickCollection::removePick(int index) {
    if (index >= 0 && index < static_cast<int>(picks.size())) {
        picks.erase(picks.begin() + index);
    }
}

void PickCollection::clear() {
    picks.clear();
}
