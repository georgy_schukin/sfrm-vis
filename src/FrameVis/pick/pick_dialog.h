#pragma once

#include "pick.h"

#include <QDialog>
#include <QListWidgetItem>
#include <QAction>

namespace Ui {
class PickDialog;
}

class Frame;
class FrameCollection;

class PickDialog : public QDialog {
    Q_OBJECT

public:
    explicit PickDialog(QWidget *parent = nullptr);
    ~PickDialog();

signals:
    void pickClicked(const Pick &pick);
    void pickChanged(const Pick &pick, const Pick &new_pick);
    void pickRemoved(const Pick &pick);

public slots:
    void setFrameCollection(const FrameCollection *frames);
    void updatePicks();

protected slots:
    void showContextMenu(const QPoint &point);

protected:
    void keyPressEvent(QKeyEvent *event) override;

private:
    void updatePicksList();

    void onPickSettings();
    void onPickDelete();

private:
    class PickListItem : public QListWidgetItem {
    public:
        PickListItem(const Pick &pick, QString text, QListWidget *parent = nullptr) :
            QListWidgetItem(text, parent),
            pick(pick) {
        }

        const Pick& getPick() const {
            return pick;
        }

    private:
        Pick pick;
    };

private:
    const PickListItem* currentItem() const;

private:
    Ui::PickDialog *ui;
    const FrameCollection *frames {nullptr};

    QAction *settings_ac {nullptr};
    QAction *delete_ac {nullptr};
};
