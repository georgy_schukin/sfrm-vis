#pragma once

#include "my_graphics_scene.h"
#include "frame/frame_data.h"
#include "pick/pick.h"

#include <QMainWindow>
#include <memory>
#include <vector>

class Frame;
class FrameCollection;
class Palette;
class Transformation;
class Interpolation;
class PickCollection;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;

public:
    enum class MouseMode {
        Drag = 0,
        AddPick,
        DeletePick
    };

signals:
    void framesAdded();
    void framesUpdated();

    void pickAdded(const Pick &pick);
    void pickRemoved(const Pick &pick);
    void pickChanged(const Pick &old_pick, const Pick &new_pick);
    void picksUpdated();
    void picksCleared();

    void currentFrameChanged(const Frame *frame);
    void currentFramePointChanged(const QPoint &point);
    //void frameCursorUnfocused();
    void interpolationChanged(const Interpolation *interp);
    void paletteChanged(const Palette *palette);
    //void pixmapsChanged();

    void mouseModeChanged(MouseMode mode);

private slots:
    void on_actionOpen_triggered();
    void on_actionAdd_triggered();
    void on_actionSave_triggered();
    void on_actionExit_triggered();

    void on_actionGenerate_frame_triggered();
    void on_actionGenerate_circle_frame_triggered();
    void on_actionGenerate_ribbon_frame_triggered();

    void on_actionAbout_triggered();

    void on_actionNext_frame_triggered();
    void on_actionPrevious_frame_triggered();

    void on_actionZoom_in_triggered();
    void on_actionZoom_out_triggered();
    void on_actionDiscard_zoom_triggered();

    void on_actionLinear_triggered();
    void on_actionLogarithmic_triggered();
    void on_actionSquare_root_triggered();
    void on_actionSettings_triggered();

    void on_actionView_Picks_triggered();
    void on_actionDelete_Current_Picks_triggered();
    void on_actionDelete_All_Picks_triggered();

    void on_actionMagnification_Zoom_In_triggered();
    void on_actionMagnification_Zoom_Out_triggered();
    void on_actionMagnification_Reset_Zoom_triggered();

    void on_actionNew_Pick_triggered();

    void on_actionPalette_Rainbow_triggered();
    void on_actionPalette_Monochrome_triggered();
    void on_actionPalette_Gnuplot_triggered();
    void on_actionPalette_Rainbow_without_Black_triggered();

    void on_actionMouseModeDrag_triggered();
    void on_actionMouseModeAdd_Pick_triggered();
    void on_actionMouseModeDelete_Pick_triggered();

protected:
    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;

private:
    void loadFramesFromFiles(const QStringList &filenames);
    void addFramesFromFiles(const QStringList &filenames);
    void saveFrameToFile(const QString &filename);

    void setSingleFrame(const FrameData &frame);

    void clearFrames();

    void addFrame(const FrameData &frame, const QString &filename = "");
    void addFrameFromFile(const QString &filename);
    void addPixmapOnlyFrame(const QPixmap &pixmap, const QString &filename = "");

    void setCurrentFramePoint(const QPoint &point);
    QPoint getCurrentFramePoint() const;

    void setCurrentFrame(Frame *frame);
    Frame* getCurrentFrame() const;

    void selectFrame(int index);

    void setCurrentPixmap(const QPixmap &pixmap);

    void clearStatus();
    void updateStatus(const QPoint &point);

    void onLoadedFrames();

    void initJumbotron();
    void updateJumbotron();

    void initInterpolation();
    void updatePixmaps();

    void updateFrameView(const Frame *frame);
    void updateWindowTitle(const Frame *frame);

    void updateMagnifyPane(const Frame *frame);

    void clearView();
    void fitPixmapToScreen();

    QStringList getOpenFileNames();

    void addPick(const QPoint &point);
    void changePick(const Pick &pick, const Pick &new_pick);
    void removePick(const Pick &pick);
    void clearCurrentPicks();
    void clearAllPicks();
    std::vector<Pick> getAllPicks() const;

    void centerOnPick(const Pick &pick);

    void setMouseMode(MouseMode mode);

    void onSceneMousePressed(const QPoint &point);
    void onPickClicked(const Pick &pick);

    void setInterpolationMin(double min);
    void setInterpolationMax(double max);
    void setInterpolationMinMax(double min, double max);

    void setInterpolation(const std::shared_ptr<Interpolation> &interpolation);
    void setColorPalette(const std::shared_ptr<Palette> &palette);

private:
    Ui::MainWindow *ui;
    MyGraphicsScene scene;

    std::shared_ptr<FrameCollection> frames;
    std::shared_ptr<Palette> current_palette;
    std::shared_ptr<Interpolation> current_interpolation;

    class QListWidget *frame_list {nullptr};
    class MagnifyPane *frame_pane {nullptr};

    Frame *current_frame {nullptr};
    QPoint current_point {-1, -1};

    MouseMode mouse_mode {MouseMode::Drag};
};

