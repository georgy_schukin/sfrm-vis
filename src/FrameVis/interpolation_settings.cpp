#include "interpolation_settings.h"
#include "ui_interpolation_settings.h"

#include <cmath>

InterpolationSettings::InterpolationSettings(QWidget *parent, double min, double max, double min_min, double max_max) :
    QDialog(parent),
    ui(new Ui::InterpolationSettings)
{
    ui->setupUi(this);

    ui->interpControls->setMinMin(min_min);
    ui->interpControls->setMaxMax(max_max);
    ui->interpControls->setMin(min);
    ui->interpControls->setMax(max);

    ui->interpControls->setVerticalOrientation(true);
    ui->interpControls->enableDynamicControls(false);
}

InterpolationSettings::~InterpolationSettings() {
    delete ui;
}

double InterpolationSettings::getCurrentMin() const {
    return ui->interpControls->getCurrentMin();
}

double InterpolationSettings::getCurrentMax() const {
    return ui->interpControls->getCurrentMax();
}

void InterpolationSettings::setMin(double value) {
    ui->interpControls->setMin(value);
}

void InterpolationSettings::setMax(double value) {
    ui->interpControls->setMax(value);
}

void InterpolationSettings::setThisFrameBounds(double min, double max) {
    this_frame_bounds = QSizeF(min, max);
}

void InterpolationSettings::setAllFramesBounds(double min, double max) {
    all_frames_bounds = QSizeF(min, max);
}

void InterpolationSettings::on_currentMinAsMin_clicked() {
    setMin(this_frame_bounds.width());
}

void InterpolationSettings::on_currentMaxAsMax_clicked() {
    setMax(this_frame_bounds.height());
}

void InterpolationSettings::on_allMinAsMin_clicked() {
    setMin(all_frames_bounds.width());
}

void InterpolationSettings::on_allMaxAsMax_clicked() {
    setMax(all_frames_bounds.height());
}
