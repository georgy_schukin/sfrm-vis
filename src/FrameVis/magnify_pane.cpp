#include "magnify_pane.h"
#include "ui_magnify_pane.h"
#include "magnify_widget.h"

#include <QScrollBar>

MagnifyPane::MagnifyPane(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MagnifyPane)
{
    ui->setupUi(this);
    magnify_layout = ui->widgetVBox;
}

MagnifyPane::~MagnifyPane() {
    clear();
    delete ui;
}

void MagnifyPane::clear() {
    for (auto *widget: magnify_widgets) {
        magnify_layout->removeWidget(widget);
        delete widget;
    }
    magnify_widgets.clear();
}

MagnifyWidget* MagnifyPane::addMagnifyWidget(int view_size) {
    auto *widget = new MagnifyWidget(0, view_size, this);
    magnify_layout->addWidget(widget);
    magnify_widgets.push_back(widget);
    //adjustScrollSize();
    return widget;
}

MagnifyWidget* MagnifyPane::getMagnifyWidget(int index) {
    if (index < 0 || index >= static_cast<int>(magnify_widgets.size())) {
        return nullptr;
    }
    return magnify_widgets[static_cast<size_t>(index)];
}

void MagnifyPane::adjustScrollSize() {
    //updateGeometry();
    int new_width = ui->scrollArea->widget()->width();
    if (ui->scrollArea->verticalScrollBar()->isVisible()) {
        new_width += ui->scrollArea->verticalScrollBar()->width();
    }
    ui->scrollArea->setFixedWidth(new_width);
}

void MagnifyPane::setFrame(int widget_index, const Frame *frame) {
    auto *widget = getMagnifyWidget(widget_index);
    if (widget) {
        widget->setFrame(frame);
    }
}

void MagnifyPane::resizeEvent(QResizeEvent *event) {
    for (auto *widget: magnify_widgets) {
        widget->makeSquareSize(widget->width());
    }
    //adjustScrollSize();
    QWidget::resizeEvent(event);
}
