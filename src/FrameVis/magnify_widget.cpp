#include "magnify_widget.h"
#include "ui_magnify_widget.h"
#include "frame/frame.h"
#include "frame/frame_data.h"
#include "my_graphics_scene.h"
#include "pick/pick.h"

#include <QGraphicsPixmapItem>
#include <QResizeEvent>
#include <cmath>

MagnifyWidget::MagnifyWidget(int zoom, int view_size, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MagnifyWidget)
{
    ui->setupUi(this);

    ui->graphicsView->setInteractive(false);
    ui->graphicsView->enableMouse(false);
    ui->graphicsView->setScene(&scene);
    ui->graphicsView->resize(view_size, view_size);

    ui->text_label->setText("");
    ui->name_label->setText("");
    ui->name_label->hide();

    connect(this, &MagnifyWidget::frameUpdated, this, &MagnifyWidget::updatePixmap);
    connect(this, &MagnifyWidget::frameUpdated, this, &MagnifyWidget::updateTitle);
    connect(this, &MagnifyWidget::frameUpdated, this, &MagnifyWidget::updateStatus);

    connect(this, &MagnifyWidget::pointUpdated, this, &MagnifyWidget::moveExistingPixmap);
    connect(this, &MagnifyWidget::pointUpdated, this, &MagnifyWidget::updateStatus);

    connect(ui->graphicsView, &MyGraphicsView::zoomChanged, [this](double zoom) {
        emit this->zoomChanged(zoom);
    });
    connect(this, &MagnifyWidget::zoomChanged, [this]() {
        this->moveExistingPixmap(this->current_point);
    });
    //connect(this, &MagnifyWidget::zoomChanged, this, &MagnifyWidget::zoomExistingPixmap);

    connect(ui->graphicsView, &MyGraphicsView::sceneMouseOver, [this](const QPoint &point) {
        emit mouseOver(point);
    });

    //adjustSize();
    //updateGeometry();
}

MagnifyWidget::~MagnifyWidget() {
    delete ui;
}

/*QSize MagnifyWidget::sizeHint() const {
    int width = 200;
    int height = width + ui->text_label->height()*2;
    return QSize(width, height);
}*/

void MagnifyWidget::setFrame(const Frame *frame) {
    current_frame = frame;
    emit frameUpdated(frame);
}

void MagnifyWidget::setCurrentPoint(const QPoint &point) {
    if (current_point != point) {
        current_point = point;
        emit pointUpdated(point);
    }
}

void MagnifyWidget::setZoom(double factor) {
    ui->graphicsView->setZoom(factor);
}

void MagnifyWidget::zoomIn() {
    ui->graphicsView->zoomIn();
}

void MagnifyWidget::zoomOut() {
    ui->graphicsView->zoomOut();
}

void MagnifyWidget::resetZoom() {
    ui->graphicsView->discardZoom();
}

void MagnifyWidget::updatePixmap() {
    scene.clearPixmap();

    if (!current_frame || !current_frame->getPixmap()) {
        return;
    }

    scene.setPixmap(current_frame->getPixmap());
    ui->graphicsView->setSceneRect(scene.getPixmapRect());
    //ui->graphicsView->fitInView(scene.getPixmapRect(), Qt::KeepAspectRatio);

    //zoomExistingPixmap();
    //moveExistingPixmap();
}

void MagnifyWidget::moveExistingPixmap(const QPoint &point) {
    ui->graphicsView->centerOn(QPointF(point));
    scene.clearPicks();
    scene.addPick(Pick(point), false);
    //scene->update(scene->sceneRect());
}

/*void MagnifyWidget::zoomExistingPixmap(double factor) {
    ui->graphicsView->setZoom(factor);
}*/

void MagnifyWidget::updateTitle() {
    if (current_frame) {
        ui->name_label->setText(current_frame->getName());
    } else {
        ui->name_label->setText("");
    }
}

void MagnifyWidget::updateStatus() {
    if (current_frame) {
        ui->text_label->setText(current_frame->strValueAtFull(current_point));
    } else {
        ui->text_label->setText("");
    }
}

void MagnifyWidget::makeSquareSize(int size) {
    setFixedHeight(size + ui->text_label->height());
    //adjustSize();
    //updateGeometry();
}

void MagnifyWidget::adjustScene() {
    ui->graphicsView->adjustScene();
}

void MagnifyWidget::resizeEvent(QResizeEvent *event) {
    ui->graphicsView->adjustScene();
    QWidget::resizeEvent(event);
}

