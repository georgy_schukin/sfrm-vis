#pragma once

#include <QGraphicsView>

class Pick;

class MyGraphicsView : public QGraphicsView {
    Q_OBJECT
public:
    MyGraphicsView(QWidget *parent = nullptr);

    void zoomIn(double factor = 1.5);
    void zoomOut(double factor = 1.5);
    void discardZoom();

    double getZoom() const {
        return scale_factor;
    }

    void enableMouse(bool enabled = true);
    void enableDrag(bool enabled = true);

    QRectF visibleSceneRect(const QSize &size) const;

signals:
    void sceneMouseOver(const QPoint &point);
    void sceneMousePressed(const QPoint &point);
    void zoomChanged(double factor);

public slots:
    void setPickMode(bool enabled);
    void centerOnPick(const Pick &pick);
    void setZoom(double factor);
    void adjustScene();

protected:
    void wheelEvent(QWheelEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;    
    void resizeEvent(QResizeEvent *event) override;

private:
    void scaleForZoom(double factor);

private:
    bool pick_mode {false};
    bool mouse_enabled {true};
    double scale_factor {1.0};
};
