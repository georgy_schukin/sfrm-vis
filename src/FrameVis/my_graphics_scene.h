#pragma once

#include <QGraphicsScene>
#include <vector>

class Pick;
class PickItem;
class QAction;

class MyGraphicsScene : public QGraphicsScene {
    Q_OBJECT
public:
    MyGraphicsScene(QObject *parent = nullptr);

    void setPixmap(const QPixmap &pixmap);
    void clearPixmap();
    bool hasPixmap() const;
    QRectF getPixmapRect() const;

    void addPick(const Pick &pick, bool interactive = true);
    void addPick(const QPoint &point, bool interactive = true);
    bool hasPick(const Pick &pick) const;
    void changePick(const Pick &old_pick, const Pick &new_pick);
    void removePick(const Pick &pick);
    void clearPicks();    

signals:
    void pickChanged(const Pick &old_pick, const Pick &new_pick);
    void pickRemoved(const Pick &pick);
    void pickClicked(const Pick &pick);

protected slots:
    void onPickContextMenu(const QPoint &pos, const Pick &pick);
    void onPickClicked(const Pick &pick);

protected:
    void onPickSettings(const Pick &pick);
    void onPickDelete(const Pick &pick);

private:
    QGraphicsPixmapItem *pixmap_item {nullptr};
    std::vector<PickItem*> pick_items;

    QAction *settings_ac;
    QAction *delete_ac;
};
