#include "my_graphics_scene.h"
#include "pick/pick.h"
#include "pick/pick_item.h"
#include "pick/pick_settings.h"

#include <QGraphicsSceneMouseEvent>
#include <QGraphicsPixmapItem>
#include <QMenu>

MyGraphicsScene::MyGraphicsScene(QObject *parent) :
    QGraphicsScene(parent)
{
    settings_ac = new QAction("Settings...", this);
    delete_ac = new QAction("Delete", this);
}

void MyGraphicsScene::setPixmap(const QPixmap &pixmap) {
    clearPixmap();
    pixmap_item = addPixmap(pixmap);
    pixmap_item->setZValue(-1);
    pixmap_item->setAcceptedMouseButtons(0);
}

bool MyGraphicsScene::hasPixmap() const {
    return (pixmap_item);
}

void MyGraphicsScene::clearPixmap() {
    if (pixmap_item) {
        removeItem(pixmap_item);
        delete pixmap_item;
        pixmap_item = nullptr;
    }
}

QRectF MyGraphicsScene::getPixmapRect() const {
    if (pixmap_item) {
        return pixmap_item->boundingRect();
    }
    return QRectF();
}

void MyGraphicsScene::addPick(const Pick &pick, bool interactive) {
    auto *item = new PickItem(pick, 20, this);
    if (interactive) {
        connect(item, &PickItem::contextMenu, this, &MyGraphicsScene::onPickContextMenu);
        connect(item, &PickItem::itemPressed, this, &MyGraphicsScene::onPickClicked);
    }
    item->setZValue(0);
    item->setFlag(QGraphicsItem::ItemIgnoresTransformations, true);
    //item->setPos(QPointF(pick.getPoint()));
    pick_items.push_back(item);
    addItem(item);    
}

void MyGraphicsScene::addPick(const QPoint &point, bool interactive) {
    addPick(Pick(point), interactive);
}

bool MyGraphicsScene::hasPick(const Pick &pick) const {
    for (auto *item: pick_items) {
        if (item->getPick() == pick) {
            return true;
        }
    }
    return false;
}

void MyGraphicsScene::changePick(const Pick &old_pick, const Pick &new_pick) {
    if (hasPick(old_pick)) {
        removePick(old_pick);
        addPick(new_pick);
    }
}

void MyGraphicsScene::removePick(const Pick &pick) {
    for (auto it = pick_items.begin(); it != pick_items.end();) {
        auto *item = *it;
        if (item->getPick() == pick) {
            removeItem(item);
            delete item;
            it = pick_items.erase(it);
        } else {
            it++;
        }
    }
}

void MyGraphicsScene::clearPicks() {
    for (auto *item: pick_items) {
        removeItem(item);
        delete item;
    }
    pick_items.clear();
}

void MyGraphicsScene::onPickContextMenu(const QPoint &pos, const Pick &pick) {
    QMenu menu;
    menu.addAction(settings_ac);
    menu.addAction(delete_ac);
    auto *action = menu.exec(pos);
    if (action == settings_ac) {
        onPickSettings(pick);
    } else if (action == delete_ac) {
        onPickDelete(pick);
    }
}

void MyGraphicsScene::onPickSettings(const Pick &pick) {
    PickSettings dlg(pick, nullptr);
    if (hasPixmap()) {
        dlg.setBounds(getPixmapRect());
    }
    if (dlg.exec() == QDialog::Accepted) {        
        emit pickChanged(pick, dlg.getPick());
    }
}

void MyGraphicsScene::onPickDelete(const Pick &pick) {
    if (hasPick(pick)) {
        removePick(pick);
        emit pickRemoved(pick);
    }
}

void MyGraphicsScene::onPickClicked(const Pick &pick) {
    emit pickClicked(pick);
}
