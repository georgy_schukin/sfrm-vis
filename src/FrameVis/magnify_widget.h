#pragma once

#include <QWidget>
#include <QPoint>

#include "my_graphics_scene.h"

class Frame;

namespace Ui {
class MagnifyWidget;
}

class MagnifyWidget : public QWidget {
    Q_OBJECT

public:
    explicit MagnifyWidget(int scale_factor = 20, int view_size = 200, QWidget *parent = nullptr);
    ~MagnifyWidget() override;

    /*bool hasHeightForWidth() const override {
        return true;
    }

    int heightForWidth(int width) const override {
        return width + 40;
    }

    QSize sizeHint() const override;*/

signals:
    void frameUpdated(const Frame *frame);
    void zoomChanged(double factor);
    void pointUpdated(const QPoint &point);
    void mouseOver(const QPoint &point);

public slots:
    void setFrame(const Frame *frame);
    void setCurrentPoint(const QPoint &point);
    void setZoom(double factor);
    void zoomIn();
    void zoomOut();
    void resetZoom();
    void makeSquareSize(int size);
    void adjustScene();

private slots:
    void updateTitle();
    void updateStatus();
    void updatePixmap();
    //void zoomExistingPixmap(double factor);
    void moveExistingPixmap(const QPoint &point);

protected:
    void resizeEvent(QResizeEvent *event) override;

private:
    Ui::MagnifyWidget *ui;

    const Frame *current_frame {nullptr};
    QPoint current_point {-1, -1};

    MyGraphicsScene scene;
};

