#include "my_graphics_view.h"
#include "pick/pick.h"

#include <QWheelEvent>
#include <cmath>

MyGraphicsView::MyGraphicsView(QWidget *parent) :
    QGraphicsView(parent) {
}

void MyGraphicsView::setPickMode(bool enabled) {
    pick_mode = enabled;
}

void MyGraphicsView::enableMouse(bool enabled) {
    mouse_enabled = enabled;
}

void MyGraphicsView::enableDrag(bool enabled) {
    setDragMode(enabled ? QGraphicsView::ScrollHandDrag : QGraphicsView::NoDrag);
}

void MyGraphicsView::zoomIn(double factor) {
    scale_factor *= factor;
    scale(factor, factor);
    emit zoomChanged(scale_factor);
}

void MyGraphicsView::zoomOut(double factor) {
    scale_factor /= factor;
    scale(1.0/factor, 1.0/factor);
    emit zoomChanged(scale_factor);
}

void MyGraphicsView::setZoom(double factor) {
    if (std::fabs(scale_factor - factor) >= 1e-8) {
        scale_factor = factor;
        scaleForZoom(scale_factor);
        emit zoomChanged(scale_factor);
    }
}

void MyGraphicsView::discardZoom() {
    setZoom(1.0);
}

void MyGraphicsView::wheelEvent(QWheelEvent *event) {    
    const auto degrees = event->angleDelta().y();
    if (degrees > 0) {
        zoomIn(1.5);
    } else if (degrees < 0) {
        zoomOut(1.5);
    }
    QGraphicsView::wheelEvent(event);
}

void MyGraphicsView::mousePressEvent(QMouseEvent *event) {
    if (!mouse_enabled) {
        return;
    }
    if (event->button() == Qt::LeftButton) {        
        auto point = mapToScene(event->pos());
        if (sceneRect().contains(point)) {
            QPoint scene_point(int(point.x()), int(point.y()));
            emit sceneMousePressed(scene_point);
        }
    }
    QGraphicsView::mousePressEvent(event);
}

void MyGraphicsView::mouseReleaseEvent(QMouseEvent *event) {
    if (!mouse_enabled) {
        return;
    }    
    QGraphicsView::mouseReleaseEvent(event);
}

void MyGraphicsView::mouseMoveEvent(QMouseEvent *event) {
    if (!mouse_enabled) {
        return;
    }
    auto point = mapToScene(event->pos());
    emit sceneMouseOver(QPoint(int(point.x()), int(point.y())));
    QGraphicsView::mouseMoveEvent(event);
}

void MyGraphicsView::resizeEvent(QResizeEvent *event) {
    //const auto visible_rect = visibleSceneRect(event->oldSize());
    //fitInView(visible_rect, Qt::KeepAspectRatio);
    QGraphicsView::resizeEvent(event);
}

QRectF MyGraphicsView::visibleSceneRect(const QSize &size) const {
    const auto top_left = mapToScene (0, 0);
    const auto bottom_right = mapToScene (size.width(), size.height());
    return QRectF(top_left, bottom_right);
}

void MyGraphicsView::centerOnPick(const Pick &pick) {
    centerOn(QPointF(pick.getPoint()));
}

void MyGraphicsView::adjustScene() {
    const auto center = mapToScene(QPoint(viewport()->width()/2, viewport()->height()/2));
    scaleForZoom(scale_factor);
    centerOn(center);
}

void MyGraphicsView::scaleForZoom(double factor) {
    fitInView(sceneRect(), Qt::KeepAspectRatio);
    scale(factor, factor);
}
